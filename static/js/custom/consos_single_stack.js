/*******************************************************************************
                    Script consos en mode single stack
*******************************************************************************/

var double_stack_mode = false;

var stack = {};

var display_stack = {};

var stack_button = {};

var transfert_from, transfert_to;

/* Pour inverser les deux noms de note dans l'onglet transfert
   (ainsi que les variables conrrespondantes */
function transfert_switch_notes() {
    var temp = transfert_from;
    transfert_from = transfert_to;
    transfert_to = temp;
    var note_emetteur = document.getElementById("transfert_emetteur");
    var emet_affiche = note_emetteur.firstChild;
    note_emetteur.removeChild(note_emetteur.firstChild);
    var note_destinataire = document.getElementById("transfert_destinataire");
    var dest_affiche = note_destinataire.firstChild;
    note_destinataire.removeChild(note_destinataire.firstChild);
    note_emetteur.appendChild(dest_affiche);
    note_destinataire.appendChild(emet_affiche);
}

function transfert_get_people_single() {
    var errlist = [];
    if (transfert_from == null) {
        errlist.push("Pas d'émetteur.");
    }
    if (transfert_to == null) {
        errlist.push("Pas de destinataire.");
    }
    if (errlist.length == 0) {
        return [true, [transfert_from], [transfert_to]];
    }
    else {
        return [false, "Transfert échoué : " + errlist.join(" ")]
    }
}

/* Fonction permettant d'effacer le nom de note de l'emetteur ou du destinataire dans l'onglet transfert */
function erase_transfert_note(isItDestinataire) {
    if (isItDestinataire) {
        ident = "transfert_destinataire";
        transfert_to = null;
    }
    else {
        ident = "transfert_emetteur";
        transfert_from = null;
    }
    var elemt = document.getElementById(ident);
    elemt.replaceChild(document.createTextNode("#??#"), elemt.firstChild);
}
