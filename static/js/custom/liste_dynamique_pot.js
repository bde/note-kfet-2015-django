/* Script JS qui permet d'afficher une liste dynamique
   des notes en faisant un search */

/** Il intéragit avec 3 éléments de la page, repérés par leur id :
 - "id_search_field" : l'élément dans lequel on récupère le terme recherche
                       et aussi où sera placé le pseudo/alias/… quand on clique sur une note
 - "liste_comptes" : l'élément (tableau) qui sera remplacé par la liste des notes obtenue
**/

/* fonctions de hl et de cliquabilité des lignes du tableau de recherche */

function GoTo(url)
{
    window.location = url;
}

/* fonction qui effectue la recherche puis appelle readData en callback */
function requestCompte(asked, callback) {
	var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            // la fonction de callback a besoin aussi de la question
			console.log(JSON.parse(xhr.responseText));
            callback(xhr.responseText, asked);
        }
    };
    xhr.open("POST", NOTE_ROOT_URL + "search_pot/" + document.getElementById("idpot").value , true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send("asked=" + encodeURIComponent(asked));

}

/* fonction appelée à la fin du timer */
function getInfoCompte() {
    var search_field = document.getElementById("id_search_field_pot");
    var asked = search_field.value;
    /* on ne fait la requête que si on a au moins un caractère pour chercher */
	$( "#liste_comptes" ).slideUp('fast');
    if (asked.length > 0) {
		requestCompte(asked, readDataPot);
	}
}

var timer;
var timer_on;
/* Fontion appelée quand le texte change (délenche le timer) */
function search_field_moved_pot() {
    if (timer_on) { // Si le timer a déjà été lancé, on réinitialise le compteur.
        clearTimeout(timer);
        timer = setTimeout(getInfoCompte, 300);
    }
    else { // Sinon, on le lance et on enregistre le fait qu'il tourne.
        timer = setTimeout(getInfoCompte, 300);
        timer_on = true;
    }
}

/* fonction de traitement des champs des résultats
   Ajoute le texte dans une case td à la ligne.
   Highlighte le résultat recherché */

// On ne recherche que sur ces champs
var highlight_fields = ["idbde", "nom", "prenom", "pseudo", "aliases", "historiques", "mail"];

function escapeRegExp(str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function createField(table_line, content, type, highlight, colonne, classNeg) {
    var highlight = typeof highlight !== 'undefined' ? highlight : null;
    var colonne = typeof colonne !== 'undefined' ? colonne : null;

    var caze = document.createElement(type);
    if (content == null) {
        content = "";
    }
    if ((highlight==null) || (highlight_fields.indexOf(colonne) == -1)) {
        caze.innerHTML = content;
    }
    else {
        var r = new RegExp(escapeRegExp(highlight), "ig");
        var match, mark;
        var indexes = [];
        while (match = r.exec(content)) {
            indexes.push([match.index, match.index + match[0].length]);
        }
        if (indexes.length == 0) {
            caze.appendChild(document.createTextNode(content));
        }
        else {
            caze.appendChild(document.createTextNode(content.slice(0, indexes[0][0])));
            indexes.push([content.length, null]);
				for (var i = 0; i < indexes.length - 1; i ++) {
					mark = document.createElement("mark");
					mark.appendChild(document.createTextNode(content.slice(indexes[i][0], indexes[i][1])));
					caze.appendChild(mark);
					caze.appendChild(document.createTextNode(content.slice(indexes[i][1], indexes[i + 1][0])));
				}
            }
        }

    // On masque certaines colonnes en fonction du viewport (Bootstrap 3 requis)
    hidden_xs = new Array('aliases', 'historiques', 'mail', 'section');
    hidden_sm = new Array('aliases', 'historiques', 'mail');

    if (type === "th") {
        caze.className += "text-center";
    }

    if (hidden_xs.indexOf(colonne) != -1) {
        caze.className += " hidden-xs";
    }

    if (hidden_sm.indexOf(colonne) != -1) {
        caze.className += " hidden-sm";
    }
	if (classNeg) {
		caze.className += classNeg;
	}

	table_line.appendChild(caze);
}

/* fonction qui traite les données à leur retour */
function readDataPot(oData, asked) {
    var [resultat, djent] = JSON.parse(oData);
	dejaEntres = djent;
	document.getElementById("compteur_entrees").textContent = Object.keys(dejaEntres).length.toString();

	var liste = document.createElement("table");
    liste.setAttribute("class", "table table-condensed table-bordered");
    // on crée la ligne de titre
	var thead = document.createElement("thead");
    var first_line = document.createElement("tr");
    thead.appendChild(first_line);
    var th_field_list = [gettext("Type"), gettext("Nom"), gettext("Prénom"), gettext("Pseudo/Alias/Inviteur"), gettext("Section"), gettext("Solde")];
    var field_list = ["type", "nom", "prenom", "pseudo", "section", "solde"];
    var colonne, valcolonne;
    for (var icol = 0; icol < field_list.length; icol++) {
        colonne = field_list[icol];
        content = th_field_list[icol];

        createField(first_line, content, "th", null, colonne);
    }
    var ligne;
    var size = resultat.length;
    if (size>0)
    {
        liste.appendChild(thead);
    }

	var tbody = document.createElement("tbody");
	tbody.style.cursor = "pointer"
	var dejaAffiche = {}; // Pour ne pas afficher 2 fois le même profil (quicksearch oblige)
	for (var i = 0, c = Math.min(size, 50); i<c; i++) {
		profil = resultat[i];
		if (!(profil["idbde"] in dejaAffiche) || (profil["was"] && profil["was"] == "pseudo")) {
			// on crée une nouvelle ligne
			ligne = document.createElement("tr");

			// on la remplit avec les champs du compte
			profil["solde"] = (profil["solde"]/100) + " €";

			if (profil["was"] && profil["was"] != "pseudo") {
				profil["pseudo"] = profil["terme"] + " <i>aka</i> " + profil["pseudo"] ;
			}

			profil["type"] = {"personne": "Adhérent", "club": "Club", "invite": "Invité"}[profil["type"]];

			var classNegatif = {};
			if (profil["negatif"] > 0) {
				if (profil["type"] == "Invité") {
					classNegatif["solde"] = classNegatif["pseudo"] = " liste_negatif" + profil["negatif"];
				} else {
					for (var j=0; j<field_list.length; j++) {
						classNegatif[field_list[j]] = " liste_negatif" + profil["negatif"];
					}
				}
			}

			for (var icol = 0, ncols = field_list.length; icol < ncols; icol++) {
				colonne = field_list[icol];
				valcolonne = profil[colonne];
				createField(ligne, valcolonne, "td", null, colonne, classNegatif[colonne]);
			}

            // Formatage des lignes

			if (profil["type"] == "Invité") {ligne.className = "invite";}

			if (profil["type"] == "Club") {ligne.className = "club";}

			if (profil["idbde"] in dejaEntres) {ligne.className = "dejaentre";}

			if (profil["idbde"] in dejaEntres || profil["type"] == "Adhérent" || profil["type"] == "Club") {
				ligne.addEventListener("click", do_entree.bind("trapped", ligne, profil, undefined));
			} else if (profil["type"] == "Invité") {
				ligne.addEventListener("click", panneau_invite.bind("trapped", ligne, profil));
			}
			ligne.id = profil["idbde"];
			// on ajoute la ligne au tableau
			if (!(profil["idbde"] in dejaAffiche) || !profil["was"]) {
				tbody.appendChild(ligne);
				dejaAffiche[profil["idbde"]] = Object.keys(dejaAffiche).length.toString();
			} else {
				var lignealias = tbody.childNodes[dejaAffiche[profil["idbde"]]];
				lignealias.parentNode.replaceChild(ligne, lignealias);
			}


		}
    }

    old_panel = document.getElementById("liste_comptes");
    var panel = document.createElement("div");
    panel.className = "panel panel-default table-responsive";

    if (size > 0)
    {
		liste.appendChild(tbody);
        panel.appendChild(liste);
    }
    else
    {
        var no_match = document.createElement("div");
        no_match.className = "alert alert-warning lead";

        var no_match_text = document.createTextNode(gettext("Aucune personne ne correspond à ta recherche."));

        no_match.appendChild(no_match_text);
        panel.appendChild(no_match);
    }

    // on lui donne le même id qu'avant pour pouvoir recommencer
    panel.setAttribute("id", "liste_comptes");
	$( panel ).hide();
	old_panel.parentNode.replaceChild(panel, old_panel);
	$( panel ).slideDown('fast');
}

function do_entree (ligne, profil, invitation) {
	if (invitation) {
		$( invitation["msg1"] ).delay(500).stop(false, true);
		$( invitation["msg2"] ).delay(500).stop(false, true);
	}

	var date = (new Date()).toLocaleString(),
		invite = (profil["type"]=="Invité"),
		responsable = (invite? parseInt(profil["ridbde"]): -100);

	var entree = {"idbde": profil["idbde"],
				  "invite": invite,
				  "activite": document.getElementById("idpot").value,
				  "date": date.replace(" à ", " "),
				  "responsable": responsable};

	var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			var [statut, msg] = JSON.parse(xhr.responseText)
			console.log(JSON.parse(xhr.responseText));
			affiche_message(ligne, entree, statut, msg);
			if (statut == "ok") {
				var compteur = document.getElementById("compteur_entrees");
				compteur.textContent = (parseInt(compteur.textContent) + 1).toString();
				dejaEntres[profil["idbde"]] = date;
				$( ligne ).animate({"backgroundColor":  "rgba(154, 154, 154, 0.42)", "color": "rgba(93, 93, 93, 0.47)"}, 500);
				if (invitation) {
					do_entree_invite(profil, invitation);
				}
			}
        }
    };
    xhr.open("POST", NOTE_ROOT_URL + "do_entree_pot/", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send("entree=" + encodeURIComponent(JSON.stringify(entree)));
}

function insertAfter(nodeParent, nodeToInsert, referenceNode) {
	if(referenceNode.nextSibling) {
		nodeParent.insertBefore(nodeToInsert, referenceNode.nextSibling);
	} else {
		nodeParent.appendChild(nodeToInsert)
	}
}

function panneau_invite (ligne, profil) {
	if (profil["idbde"] in dejaEntres) {
		do_entree(ligne, profil);

	} else {
		var oldPanneau =  document.getElementsByClassName("panneau" + profil["idbde"]);
		if (oldPanneau.length > 0) {
			for (var i=0; i<oldPanneau.length; i++) {
				$( oldPanneau[i] ).stop(false, true);
			}
		} else {
			var responsable = document.createElement("tr");
			responsable.setAttribute("class", "panneau" + profil["idbde"]);
			responsable.style.fontSize = "0px";
			responsable.style.backgroundColor = "rgba(214, 149, 53, 0.78)";

			var colonne = {}
			respo = {"type": "Responsable", "nom": profil["rnom"], "prenom": profil["rprenom"], "pseudo": profil["pseudo"], "solde": profil["solde"]};
			for (var champ in respo) {
				colonne[champ] = document.createElement("td");
				colonne[champ].appendChild(document.createTextNode(respo[champ]));
				colonne[champ].style.border = "0px";
				responsable.appendChild(colonne[champ]);
			}
			insertAfter(ligne.parentNode, responsable, ligne);

			var choix = document.createElement("tr");
			choix.setAttribute("class", "panneau" + profil["idbde"]);
			choix.style.fontSize = "0px";
			choix.style.backgroundColor = "rgba(214, 149, 53, 0.78)";

			var cellules = [];
			for (var i=0; i<5; i++) {
				cellules.push(document.createElement("td"));
				cellules[i].style.border = "0px";
				choix.appendChild(cellules[i]);
			}

			var note = document.createElement("button");
			var cash = document.createElement("button");
			var CB = document.createElement("button");
			cellules[3].appendChild(document.createTextNode("Règlement :"))
			note.appendChild(document.createTextNode("Note de l'inviteur"))
			cash.appendChild(document.createTextNode("Invité Cash"))
			CB.appendChild(document.createTextNode("Invité CB"))
			note.style = "width: 50%; line-height: 2em;";
			cash.style = CB.style = "width: 25%; line-height: 2em;";
			note.addEventListener("click", do_entree.bind("trapped", ligne, profil, {msg1: responsable, msg2: choix, pay: "note"}));
			cash.addEventListener("click", do_entree.bind("trapped", ligne, profil, {msg1: responsable, msg2: choix, pay: "especes"}));
			CB.addEventListener("click", do_entree.bind("trapped", ligne, profil, {msg1: responsable, msg2: choix, pay: "cb"}));

			cellules[3].appendChild(document.createElement("br"));
			cellules[3].appendChild(note);
			cellules[3].appendChild(cash);
			cellules[3].appendChild(CB);
			cellules[3].style.textAlign = "left";
			cellules[3].style.textShadow = "0px 0.1px 0px rgba(0,0,0,.5)";

			insertAfter(responsable.parentNode, choix, responsable);

			$( responsable ).animate({fontSize: "14px"}).delay(60000).animate({fontSize: "0px"}, function () {responsable.parentNode.removeChild(responsable);});
			$( choix ).animate({fontSize: "15px"}).delay(60000).animate({fontSize: "0px"}, function () {choix.parentNode.removeChild(choix);});
		}
	}
}


function do_entree_invite (profil, invitation) {
	/* invitation["pay"] contient le moyen désiré de paiement : note, especes, cb
	   Si != note, on fait le crédit ad-hoc PUIS la conso, sinon juste la conso.*/

	function do_credit_bouton (pay) {
		/* Requête ajaj à l'aide jQuery (à généraliser à tout le code peut être)*/
		jQuery.ajax({
			url: NOTE_ROOT_URL + "do_credit/",
			type: "POST",
			data: ({idbde: profil["ridbde"],
					montant: 5,
					commentaire: profil["prenom"] + " " + profil["nom"],
					type: pay,
					nom: profil["rnom"],
					prenom: profil["rprenom"],
					banque: ""}),
			dataType: "text",
			success:
				function(msg) {
					/* On traite les éventuelles erreurs */
					try {
						var answer = JSON.parse(msg);
						var cod = answer["retcode"];
					}
					catch(err) {
						display_error(msg);
						return;
					}
					if (isSuccessCode(cod)) {
						do_bouton(); // De cette manière on est sur qu'on fait d'abord le crédit et ensuite la conso.
					} else {
						display_error(answer["errmsg"]);
						return;
					}
				}
			}
		);
	}

   function do_bouton () {
		jQuery.ajax({
			url: NOTE_ROOT_URL + "do_conso/",
			type: "POST",
			data: {consodata: "(1542," + profil["ridbde"] +",1)"}, //1542 correspond au bouton "Invitation [Pot] (5 euros)"
			dataType: "text",
			success:
				function(msg) {
					/* On traite les éventuelles erreurs */
					try {
						var answer = JSON.parse(msg);
						var cod = answer["retcode"];
					}
					catch(err) {
						display_error(msg);
						return;
					}
					if (!isSuccessCode(cod)) {
						display_error(answer["errmsg"]);
						return;
					}
				}
			}
		);
	}

	if (invitation["pay"] == "note") {
		do_bouton();
	} else {
		do_credit_bouton(invitation["pay"]);
	}
}


function affiche_message(ligne, entree, type, msg) {
	var oldmessage = document.getElementById("message" + entree["idbde"]);
	if (oldmessage) {
		$( oldmessage ).stop(false, true);
	}
	var message = document.createElement("tr");
	message.setAttribute("id", "message" + entree["idbde"]);
	message.style.backgroundColor = {"ok": "#DFF0D8", "dejaentre": "#FF9F9F", 'negatif': "#FF9F9F", 'noadh': "#FF9F9F", 'club': "#FF5F5F", 'pasouvert': "#FF5F5F"}[type];
	message.style.fontSize = "0px"

	var cellules = [];
	for (var i=0; i<5; i++) {
		cellules.push(document.createElement("td"));
		cellules[i].style.border = "0px";
		message.appendChild(cellules[i]);
	}

	lienreadhesion = NOTE_ROOT_URL + "readhesions/" + entree["idbde"] + "/" + entree["activite"]

	cellules[3].innerHTML = msg + (type=="noadh"? '<br><a href="' + lienreadhesion + '">Réadhérer</a>': "");
	cellules[3].style.textAlign= "center";

	insertAfter(ligne.parentNode, message, ligne);

	$( message ).animate({fontSize: "14px"}, 'fast').delay((type=="ok"? 1000: 5000)).animate({fontSize: "0px"}, 'fast', function () {message.parentNode.removeChild(message);});

}

