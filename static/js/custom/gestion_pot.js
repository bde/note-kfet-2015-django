/* Quelques commandes appelées au chargement de la page afin de mettre en forme correctement tout en utilisant le code préexistant sur la note. */

// Etend la fenetre sur toute la largeur de la page
/*var container = document.getElementById('idpot').parentNode
container.className = container.className.replace('col-sm-10', 'col-sm-12')*/

// Charge le compteur d'entrées
$( "#nompot" ).animate({opacity: 1}, 1000)
requestCompte("zkihu", function (rep, asked){
	var dejaEntres = JSON.parse(rep)[1]
	nbEntrees = Object.keys(dejaEntres).length
	var compteur = document.getElementById("compteur_entrees")
	compteur.textContent = nbEntrees.toString()
	$( compteur.parentNode ).delay(500).animate({opacity: 1}, 1000)
	})
// Pour charger l'historique
refreshHistorique();

// On connecte l'événement "le type de paiement change" avec la fonction bank_or_not_bank.
typeMoney = document.getElementById("id_credit_form-type");
typeMoney.addEventListener("change", bank_or_not_bank);
bank_or_not_bank(); // Par défaut on n'affiche pas les champs (cf fonction).

// On met en forme le formulaire des transferts pour mieux convenir à la page
for (var i=0; i<2; i++) {
	elemt = ["montant", "commentaire"][i];
	champ = document.getElementById("id_transfert_form-" + elemt).parentNode;
	champ.firstChild.className += "hidden"; // On cache le label
	champ.lastChild.placeholder = ["Montant", "Motif"][i] + " du transfert"; // Information sur la qualité du champ
}

/* Fonction appelée lors du clic sur un des onglets à gauche. Elle s'occupe de modifier l'affichage de la page et l'état des variables associées.
   La variable onglet correspond à l'onglet sur lequel on a cliqué. */
function set_conso(onglet) {

	ongletParent = onglet.parentNode

	// Si on est déjà sur la page en question, on ne fait rien
    if (ongletParent.className == "active") {
        return 0;
    }

    // Changement de l'état des l'onglet
    ongletFrom = document.querySelector("#applets li.active").firstChild
    ongletFrom.parentNode.className = ""
    ongletParent.className = "active";

    // Affichage des éléments correspondants et dissimulation des anciens.
	var newCategorie = onglet.className;
	suppr = document.querySelectorAll("div." + ongletFrom.className);

	for (var i=0; i<suppr.length; i++) {
		suppr[i].className += " hidden";
	}

	add = document.querySelectorAll("div." + newCategorie);

	for (var i=0; i<add.length; i++) {
		add[i].className = add[i].className.replace(" hidden","");
	}

	if (newCategorie != "entree") {
		double_stack_mode = true;
	} else {
		double_stack_mode = false;
	}
}

/* Fonction appelée à l'envoi du formulaire de transfert avec le paramètre nomPot qui correspond au nom du [Pot] en cours. Si le motif n'a pas été précisé, on met le nom du pot, puis on fait appel à la fonction transferer qui se charge du reste. */
function transfert_pot() {

	motif = document.getElementById("id_transfert_form-commentaire");
	if (motif.value.length == 0) {
		motif.value = document.getElementById("nompot").textContent;
	}

	transferer()
}


/* Fonction appelée au changement du type de paiement dans l'onglet Crédit. Elle affiche ou cache les champs nom, prenom et banque selon le type de paiement. */
function bank_or_not_bank() {

	if (this.value == "cheque" || this.value == "virement") {
		var newClass = "";
	} else {
		var newClass = "hidden";
	}

	// Pas besoin des champs nom, prénom et banques si on ne paie pas pas chèque/virement
	for (var i=0; i<3; i++) {
		champ = ["nom", "prenom", "banque"][i];
		paragraph = document.getElementById("id_credit_form-" + champ).parentNode;
		paragraph.className = newClass;
	}
}
