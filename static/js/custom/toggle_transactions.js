/* Fonction qui appelle la fonction de (dé)validation pour changer l'état de la BDD et lance ensuite l'affichage du message.
Elle est appellée par n'importe quel bouton de (dé)validation sur l'événement onclick.
Elle prend en paramètre button qui est la balise <a> qui correspond à la transaction concernée.
Elle envoie l'id de la transaction et la nature de l'action (validation dévalidation) et récupère les retours : messages, code de retour, messages d'erreurs */
function validedevalide(button) {
	var ligne = button.parentNode.parentNode
	if (document.getElementById("message" + ligne.id)) {
			return;
	}

    // On détermine l'action à faire : si elle est déjà valide, il faut la dévalider
    var devalider = (button.className == "glyphicon glyphicon-ok"); //On fait confiance à l'icone : on ne devrait pas (i.e. à faire côté serveur)

    var xhr = getXMLHttpRequest(); // Préparation de la requête

    xhr.onreadystatechange = (function () {
        if (xhr.readyState == XMLHttpRequest.DONE) { // Quand la requête est terminée
            if (xhr.status == 200 || xhr.status == 0) { // Si tout s'est bien passé

                var rep = JSON.parse(xhr.responseText); // On parse le retour
                var msg = rep["msg"], retcode = rep["retcode"], errmsg = rep["errmsg"]; // On récupère les données

                if (retcode == 0) { // Si le serveur a réussi à (dé)valider la transation
                    // On change l'icone ainsi que le fond de la cellule
                    button.parentNode.className = "bg-" + (devalider ? "danger" : "success");
                    button.className = "glyphicon glyphicon-" + (devalider ? "remove" : "ok");
                    // On affiche le message
                    affmessagebis(ligne, true, msg, devalider);

                } else { // Sinon on affiche la raison
                    affmessagebis(ligne, false, errmsg, true);
                }
            } else { // Si il y a eu un pb dans la requête on affiche un problème serveur (c'est la faute des devs)
                affmessage(ligne, false, xhr.status + ": " + xhr.statusText);
            }
        }
    });

    data = ligne.id + "," +  devalider; // Encodage de la requête POST

    xhr.open("POST", NOTE_ROOT_URL + "consos/toggle_transaction", true); // Page AJAJ qui va s'occupper de traiter la requête
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send("data=" + encodeURIComponent(data)); // On envoie les données
};

/* Cette fonction permet simplement d'ajouter nodeToInsert après referenceNode
dans nodeParent (ici une ligne après une autre dans la tableau) */

function insertAfter(nodeParent, nodeToInsert, referenceNode) {
	if(referenceNode.nextSibling) {
		nodeParent.insertBefore(nodeToInsert, referenceNode.nextSibling);
	} else {
		nodeParent.appendChild(nodeToInsert);
	}
}

/*Cette fonction a pour but d'afficher le message de retour du serveur en rajoutant une ligne sous la transaction
    ligne : objet contenant la lignede la transaction sur laquel on a cliqué (contient entre autre l'id de la transaction)
    reussite : booleen signifiant si oui ou non la requête a aboutie (la transaction a bien été (dé)validé ou non)
    msg : retour du serveur (erreur ou successcode du type "Bien (dé)validé !")
	devalider : booleen signifiant si on vient de dévalider (true) ou non.
*/

function affmessagebis(ligne, reussite, msg, devalider) {
	var message = document.createElement("tr");

	message.setAttribute("id", "message" + ligne.id);
	if (reussite) {
		message.className = (devalider ? "bg-danger" : "bg-success");
	} else {
		message.style.backgroundColor = "#B45555";
		message.style.fontWeight = 'bold';
	}
	message.style.fontSize = "0px";

	var cellules = [];
	for (var i=0; i<8; i++) {
		cellules.push(document.createElement("td"));
		cellules[i].style.border = "0px";
		message.appendChild(cellules[i]);
	}

	cellules[6].appendChild(document.createTextNode(msg));
    cellules[(reussite?7:5)].innerHTML = '<div class=' + (devalider ?'"glyphicon glyphicon-remove"' : '"glyphicon glyphicon-ok"') + ' aria-hidden="true"' + (reussite? '': 'style="cursor:pointer"') + '></div>';

	if (!reussite) {
		cellules[5].firstChild.onclick = function () {$( "#message" + ligne.id ).stop(false, true);};
	}

	insertAfter(ligne.parentNode, message, ligne);

	$( message ).animate({fontSize: "12.8px"}, 'fast').delay((reussite? 2500: 30000)).animate({fontSize: "0px"}, 'fast', function () {message.parentNode.removeChild(message);});

}
