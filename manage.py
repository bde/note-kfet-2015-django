#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import os
import sys
# À partir de Django 1.7, on doit faire le setup si on veut pouvoir
# importer les modules avant d'avoir chargé toutes les apps
# Et on a besoin de connaître le nom du module settings pour cela

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "note.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    execute_from_command_line(sys.argv)
