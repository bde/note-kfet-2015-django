#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Associe chaque url à une fonction de :py:mod:`note.views` qui
    est chargée de générer la page correspondante.
"""

from django.conf.urls import include, url

# Import pour la traduction du javascript
from django.views.i18n import javascript_catalog
import note.views
import note.ajaj
#: Liste des patterns d'url
urlpatterns = [
    # pages de base
    url(ur'^/*$', note.views.login_page ,name='login_page'),
    url(ur'^/index/*$', note.views.index,name='index'),
    url(ur'^/logout/*$', note.views.logout,name='logout'),
    # Page pour changer la langue
    url(ur'^i18n/', include('django.conf.urls.i18n')),
    # consos
    url(ur'^/consos(?P<double>-double)?/*$', note.views.consos,name='consos'),
    # dons
    url(ur'^/(?:virements|dons)/*', note.views.dons,name='dons'),
    # les activités et invitations
    url(ur'^/(?:activite|invitation)s?(?P<admin>/admin)?(?P<old>/old)?/*$', note.views.activites,name='activites'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/gestion(?P<validation>/validate|/invalidate|/delete|/open|/close)?/*$', note.views.activite_gestion,name='activite_gestion'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/gestion/modifier/*$', note.views.activite_gestion_modifier,name='activite_gestion_modifier'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)(?P<admin>/admin)?/*$', note.views.activite,name='activite'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/entree/*$', note.views.activite_entree, name='activite_entree'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/del_invite/(?P<idinv>[^/]*)(?P<admin>/admin)?/*$', note.views.del_invite,name='del_invite'),
    # Afficher la liste des invités qui sont rentrés au pot
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/liste_invites/(?P<admin>/admin)?/*$', note.views.liste_invites_entres,name='liste_invites_entres'),
    # mes_activités = création d'activités
    url(ur'^/mes_activites(?:/(?P<idact>[^/]*))?(?P<delete>/delete)?/*$', note.views.mes_activites,name='mes_activites'),
    # gestion des pots
    url(ur'^/pot_gestion/*$',note.views.pot_gestion, name='pot_gestion'),
    # la recherche et gestion des comptes
    url(ur'^/comptes_advanced/*$', note.views.comptes_advanced,name='comptes_advanced'),
    url(ur'^/search_historique_pseudo/*$', note.views.search_historique_pseudo,name='search_historique_pseudo'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/*$', note.views.comptes,name='comptes'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique/(?P<num_page>\d+)/*$', note.views.historique_transactions,name='historique_transactions'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique/*$', note.views.historique_transactions,name='historique_transactions'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier(/listedroits)?/*$', note.views.modifier_compte,name='modifier_compte'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/supprimer/*$', note.views.supprimer_compte,name='supprimer_compte'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier/password/*$', note.views.password,name='password'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier/photo/*$', note.views.update_photo,name='update_photo'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/aliases/*$', note.views.aliases,name='aliases'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/aliases/delete(?P<delall>_all)?/(?P<idalias>[^/]*)/*$', note.views.unalias,name='unalias'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique_pseudo/*$', note.views.historique_pseudo,name='historique_pseudo'),
    #les réadhésions
    url(ur'^/readhesions/(?P<idbde>[^/]*)/?(?P<idact>[^/]*)/*$', note.views.readhesions,name='readhesions'),
    # la gestion des boutons
    url(ur'^/boutons/*$', note.views.boutons,name='boutons'),
    url(ur'^/boutons/(?P<idbouton>[^/]*)(?P<delete>/delete)?/*$', note.views.boutons,name='boutons'),
    # les préinscription
    url(ur'^/preinscriptions?/*$', note.views.preinscriptions,name='preinscriptions'),
    # les inscriptions
    url(ur'^/inscriptions?(?:/(?P<preid>[^/]*))?(?P<fromwei>/from_wei)?(?P<delete>/delete)?/*$', note.views.inscriptions,name='inscriptions'),
    # Application WEI
    url(ur'^/wei/*$', note.views.WEIaccueil,name='WEIaccueil'),
    url(ur'^/wei/monInscription/*$', note.views.WEImonInscription,name='WEImonInscription'),
    url(ur'^/wei/1a/*$', note.views.WEI1A,name='WEI1A'),
    url(ur'^/wei/formulaire/*$', note.views.WEI1Aalgo, name='WEI1Aalgo'),
    url(ur'^/wei/vieux/*$', note.views.WEIvieux,name='WEIvieux'),
    url(ur'^/wei/admin/*$', note.views.WEIAdmin,name='WEIAdmin'),
    url(ur'^/wei/inscrits/*$', note.views.WEIinscrits,name='WEIinscrits'),
    url(ur'^/wei/inscrits/readherer/(?P<idwei>\d+)/*', note.views.WEIreadherer,name='WEIreadherer'),
    url(ur'^/wei/inscrits/(?P<idwei>\d+)/*$', note.views.WEIchangeInscription,name='WEIchangeInscription'),
    url(ur'^/wei/inscrits1a/*$', note.views.WEIinscrits1A,name='WEIinscrits1A'),
    url(ur'^/wei/inscrits1a/(?P<idwei>\d+)/*$', note.views.WEIchangeInscription1A,name='WEIchangeInscription1A'),
    url(ur'^/wei/inscrits1a/adherer/(?P<idwei>\d+)/*$', note.views.WEIcreerCompte1A,name='WEIcreerCompte1A'),
    # interface trésorerie
    url(ur'^/tresorerie/*$', note.views.TresorerieAccueil,name='TresorerieAccueil'),
    url(ur'^/tresorerie/cheques/*$', note.views.TresorerieCheques,name='TresorerieCheques'),
    url(ur'^/tresorerie/remises/*$', note.views.TresorerieRemises,name='TresorerieRemises'),
    url(ur'^/tresorerie/(?P<idtransaction>[^/]*)/(?P<action>ajout|delete)/*$', note.views.TresorerieAjoutRemise,name='TresorerieAjoutRemise'),
    url(ur'^/tresorerie/remises/(?P<idremise>[^/]*)?(?P<clore>/clore)?/*$', note.views.TresorerieCloreRemise,name='TresorerieCloreRemise'),
    url(ur'^/tresorerie/facturation/*$', note.views.TresorerieFacturation,name='TresorerieFacturation'),
    # regeneration du password
    url(ur'^/regen_pw/(?P<token>[^/]*)/*$', note.views.regen_pw,name='regen_pw'),
    # easter egg
    url(ur'^/(?:teapot|the|tea|coffee|cafe)/*$', note.views.teapot,name='teapot'),
    # Page de liste des droits
    url(ur'^/listedroits/*$',note.views.liste_droits, name='liste_droits'),
]

urlpatterns += [
    # les pages de requêtes AJAJ
    url(ur'^/quick_search_(?P<mode>basic|dons)/*$', note.ajaj.quick_search,name='quick_search'),
    url(ur'^/search/*$', note.ajaj.search,name='search'),
    url(ur'^/search_readhesion/*$', note.ajaj.search_readhesion,name='search_readhesion'),
    url(ur'^/search_pot/(?P<idpot>[^/]*)/*$', note.ajaj.search_pot, name='search_pot'),
    url(ur'^/stats_pot/(?P<idpot>[^/]*)/*$', note.ajaj.stats_pot, name='stats_pot'),
    url(ur'^/get_boutons/(?P<flags>[^/]*)/*$', note.ajaj.get_boutons,name='get_boutons'),
    url(ur'^/get_display_info/*$', note.ajaj.get_display_info,name='get_display_info'),
    url(ur'^/get_photo/(?P<idbde>[^/]*)/*$', note.ajaj.get_photo,name='get_photo'),
    url(ur'^/do_conso/*$', note.ajaj.do_conso,name='do_conso'),
    url(ur'^/do_(?P<action>credit|retrait)/*$', note.ajaj.do_credit_retrait,name='do_credit_retrait'),
    url(ur'^/do_transfert/*$', note.ajaj.do_transfert,name='do_transfert'),
    url(ur'^/do_entree_pot/*$', note.ajaj.do_entree_pot, name='do_entree_pot'),
    url(ur'^/consos/toggle_transaction/*$', note.ajaj.toggle_transaction,name='toggle_transaction'),
]



js_info_dict = {
    'packages': ('note'),
}

# Pour la traduction du javascript
urlpatterns += [
    url(r'^jsi18n/$', javascript_catalog, js_info_dict,name='javascript_catalog'),
]
