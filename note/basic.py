#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Définition de petites choses utilisées partout.
   
   Ce module n'a aucune autre dépendance que les librairies standard et les :py:mod:`settings`.
   """

import subprocess
import re

# Les paramètres django
import settings

# L'objet HttpResponseRedirect qu'on va surcharger
from django.http import HttpResponseRedirect

class FallbackException(Exception):
    """Classe de base des exceptions qui vont entraîner une redirection vers une URL
       si elles sont levées.
       
       Elles ont toutes un attribut ``fallback_with`` qui est cette URL.
       """
    def __init__(self, fallback):
        super(FallbackException, self).__init__()
        self.fallback_with = fallback

class NotFound(FallbackException):
    """Levée quand on cherche à _get un objet qui n'existe pas (un compte, un alias, une activité…)."""
    pass

class IllegalId(FallbackException):
    """Levée quand un _cast_as d'un id échoue."""
    pass


def _get_versions():
    """Renvoie un tuple contenant les versions de nginx et django."""
    proc = subprocess.Popen(["nginx", "-v"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err = proc.communicate()[1]
    nginxversion = re.findall("nginx version: (.*)", err)
    nginxversion = nginxversion[0] if nginxversion else None
    return (nginxversion, django.get_version())

def _fundamental_variables():
    """Renvoie un dictionnaire contenant les variables incontournables qu'on doit avoir pour render un template."""
    return {"NOTE_ROOT_URL" : settings.NOTE_ROOT_URL,
            "NOTE_LOGIN_URL" : settings.NOTE_LOGIN_URL,
            "DEV" : settings.DEV,
            "PROD" : settings.PROD,
           }

def get_client_ip(request):
    """Récupère l'IP du client qui parle à Django."""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
