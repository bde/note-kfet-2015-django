#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Définitions des views.

   Une fonction par page servie.
   """

### Imports
import django

# Modules standard utiles
import json
import base64
import subprocess
import os
import shutil
import algo1a
from django.shortcuts import redirect
from datetime import datetime
from tempfile import mkdtemp

# Les objets de réponse HTTP
from django.http import HttpResponse, HttpResponseRedirect, Http404
# Pour renvoyer facilement un template
from django.shortcuts import render, get_object_or_404
from django.template import RequestContext, Context, TemplateDoesNotExist
from django.template.loader import render_to_string
# Pour html-escaper, notamment
import django.utils.html

# Pour protéger les données sensibles dans un POST en mode DEBUG
from django.views.decorators.debug import sensitive_post_parameters

# Les formset pour avoir plusieurs fields() identiques dans un form()
from django.forms import formset_factory

#: Import pour la traduction
from django.utils.translation import ugettext_lazy as _

# Les paramètres django
import settings

# Les formulaires
import forms
# Les messages
import messages
# La communication avec le backend
import nk
# Les utilitaires
import utilities
# Des utilitaires pour lesquels on n'a pas envie de rappeler ``module.`` à chaque fois
from utilities import standard_page, standard_page_withignores
# Gestion des requêtes AJAJ
import ajaj

#: Ce module contient les valeurs qu'on a besoin de conserver
#: d'une requête HTTP du client à une autre
import keep_alive

import basic

@sensitive_post_parameters('password')
def login_page(request):
    """Renvoie la page de login ou traite les données du formulaire de login"""
    if request.method == "POST":
        # on récupère le formulaire
        form = forms.LoginForm(request.POST, label_suffix=" :")
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            masque_droits = form.cleaned_data["droits"]
            masque_droits = settings.ACL_MASKS[masque_droits][1]
            # On tente un login
            return nk.login_NK(request, username, password, form, masque_droits)
    else:
        form = forms.LoginForm(label_suffix=" :")
    variables = basic._fundamental_variables()
    variables["form"] = form
    return render(request, "note/login.html", variables)

def index(request):
    """La page qui ne sert à rien"""
    # On peuple les variables, sans demander la socket
    success, sock_ou_response, variables = utilities.get_varsock(request)
    if success:
        return render(request, "note/index.html", variables)
    else:
        # Une erreur a eu lieu
        response = sock_ou_response
        return response

def logout(request):
    """Fonction de déconnexion"""
    if request.session.get("logged", None) == "ok": # Il est possible qu'en fait on ait déjà timeout, donc pas besoin de vraiment se déloguer.
        # On enlève logged du cookie Django
        request.session["logged"] = "no"
        # Il faut fermer la socket
        success, sock_ou_response = nk.socket_still_alive(request)
        if not success:
            return sock_ou_response
        sock = sock_ou_response
        sock.write(json.dumps(["exit"]))
        sock.close()
        # On supprime la socket de keep_alive.CONNS
        idbde = request.session["whoami"]["idbde"]
        del keep_alive.CONNS[idbde]
        # On renvoie sur la page de login
        messages.add_success(request, messages.SUCCMSG_LOGOUT)
    return HttpResponseRedirect(settings.NOTE_LOGIN_URL)

@sensitive_post_parameters("password", "password_confirm")
def regen_pw(request, token):
    """Page pour demander l'envoie par mail d'un token de changement de mot de passe
       et pour changer le mot de passe quand on vient avec ce token."""
    variables = basic._fundamental_variables()
    if token:
        if request.method == "POST":
            form = forms.PasswordForm(request.POST, label_suffix=" :")
            if form.is_valid():
                try:
                    sock = nk.connect_NK(request)
                except nk.NKError as exc:
                    return nk.gerer_NKError(request, exc)
                new_pass = form.cleaned_data["password"]
                data = [token, new_pass]
                sock.write(json.dumps(["confirm_reset_password", data]))
                out = nk.full_read(sock)
                errors = out["errmsg"]
                if errors:
                    messages.add_error(request, errors)
                else:
                    messages.add_success(request, out["msg"])
        else:
            form = forms.PasswordForm(label_suffix=" :")
        variables["form"] = form
        return render(request, "note/regen_pw.html", variables)
    else :
        if request.method == "POST":
            form = forms.Regen_pwForm(request.POST, label_suffix=" :")
            if form.is_valid():
                try:
                    sock = nk.connect_NK(request)
                except nk.NKError as exc:
                    return nk.gerer_NKError(request, exc)
                nom = form.cleaned_data["nom"]
                prenom = form.cleaned_data["prenom"]
                mail = form.cleaned_data["mail"]
                data = [prenom,nom,mail]
                sock.write(json.dumps(["generate_reset_password", data]))
                out = nk.full_read(sock)
                errors = out["errmsg"]
                if errors:
                    messages.add_error(request, errors)
                else:
                    messages.add_success(request, out["msg"])
        else:
            form = forms.Regen_pwForm(label_suffix=" :")
        variables["form"] = form
        return render(request, "note/ask_regen_pw.html", variables)


@standard_page
def consos(request, sock, kwargs):
    """La page des consos.

       Dans ``kwargs`` :
        * ``double="-double"`` si on a demandé le mode conso double
        """
    variables = {"double_stack_mode" : kwargs["double"], "page_consos" : True}
    categories = utilities._get_boutons_categories(sock, request, False)
    variables["categories"] = categories
    boutons = utilities._get_boutons(sock, request)
    variables["boutons"] = [(categ, [b for b in boutons if b["categorie"] == categ]) for categ in categories]
    sock.write(json.dumps(["historique_transactions", "last"]))
    out = nk.full_read(sock)
    if nk._is_success_code(out["retcode"]):
        variables["historique"] = out["msg"]
    else:
        messages.add_error(request, out["errmsg"])
    sock.write(json.dumps(["mayi", "transferts"]))
    variables["hastransfert"] = nk.full_read(sock)["msg"]
    sock.write(json.dumps(["mayi", "credits"]))
    variables["hascredit"] = nk.full_read(sock)["msg"]
    sock.write(json.dumps(["mayi", "retraits"]))
    variables["hasretrait"] = nk.full_read(sock)["msg"]
    # Le formulaire de Crédit
    variables["credit_form"] = forms.CreditRetraitForm(prefix="credit_form", label_suffix=" :")
    # Le formulaire de Retrait
    variables["retrait_form"] = forms.CreditRetraitForm(prefix="retrait_form", label_suffix=" :")
    # Le formulaire de Transfert
    variables["transfert_form"] = forms.TransfertForm(prefix="transfert_form", label_suffix=" :")
    return (variables, "note/consos.html")

@standard_page
def dons(request, sock, kwargs):
    """La page des dons"""
    variables = {}
    # Le formulaire de Don
    variables["don_form"] = forms.TransfertForm(prefix='transfert_form', label_suffix=" :")
    return (variables, "note/dons.html")

@standard_page
def activites(request, sock, kwargs):
    """Affichage des activités

       Dans ``kwargs`` :
        * ``admin`` qui vaut ``"/admin"`` si on a demandé le mode admin
        * ̀ `old``   qui vaut ``"/old"``   si on a demandé le mode old
        """
    admin, old = kwargs["admin"], kwargs["old"]
    asked_admin = (admin == "/admin")
    asked_old = (old == "/old")
    # on récupère les droits de l'user
    sock.write(json.dumps(["mayi", "full_rights"]))
    rights = nk.full_read(sock)["msg"]
    # on regarde si on certains droits
    hasadmin = ("activites_admin" in rights["droits"])
    hasnote = ("note" in rights["droits"])
    # on est en mode administration si on en a le droit ET qu'on l'a demandé
    isadmin = asked_admin and hasadmin
    # on est en mode affichage_old  si on en a le droit ET qu'on a demandé old
    isold = asked_old and hasadmin
    if request.method == "POST":
        return HttpResponse("Bad Request", status=400)
    else:
        liste_activites = utilities._get_activites(sock, isadmin, request, isold=isold)
        # On affiche la liste des activités en ajoutant les variables standard
        variables = {"activites": liste_activites,
                     "hasadmin": hasadmin,
                     "isadmin": isadmin,
                     "isold": isold,
                     "hasnote": hasnote}
        return (variables, "note/activites.html")

@standard_page
def activite(request, sock, kwargs):
    """Affichage d'une activité pour y inviter

       Dans ``kwargs`` :
        * ``idact``
        * ``admin`` qui vaut ``"/admin"`` si on a demandé le mode admin
        """
    idact, admin = kwargs["idact"], kwargs["admin"]
    asked_admin = (admin == "/admin")
    # On demande si on le droit d'être admin
    sock.write(json.dumps(["mayi", "activites_admin"]))
    hasadmin = nk.full_read(sock)["msg"]
    # On est en mode administration si on en a le droit ET qu'on l'a demandé
    isadmin = asked_admin and hasadmin
    # On récupère l'activité
    activite = utilities._get_activite(sock, idact, request)
    if request.method == "POST":
        form = forms.InviteForm(request.POST, label_suffix=" :")
        if form.is_valid():
            nom = form.cleaned_data["nom"]
            prenom = form.cleaned_data["prenom"]
            data = [nom, prenom, idact]
            if isadmin: # un admin doit préciser le reponsable (en cas d'échec ce sera lui le responsable)
                try:
                    data.append(int(request.POST["idrespo"]))
                except:
                    pass
            sock.write(json.dumps(["add_invite", [data, "A" * isadmin]]))
            out = nk.full_read(sock)
            erreur = out["errmsg"]
            if erreur:
                if out["retcode"] == 110:
                    messages.add_warning(request, erreur)
                else:
                    messages.add_error(request, erreur)
            else:
                messages.add_success(request, messages.SUCCMSG_ADDINV)
                return HttpResponseRedirect("{}activites/{}{}/".format(settings.NOTE_ROOT_URL, idact, "/admin" * isadmin))

    else:
        form = forms.InviteForm(label_suffix=" :", initial=activite)
    liste_invites = utilities._get_invites(sock, idact, isadmin, request)
    # on prépare les variables
    variables = {"activite": activite,
                 "form": form,
                 "liste_invites": liste_invites,
                 "hasadmin": hasadmin,
                 "isadmin": isadmin}
    return (variables, "note/invitation.html")

@standard_page
def activite_gestion(request, sock, kwargs):
    """Page de gestion d'une activité.

       Dans ``kwargs`` :
        * ``idact`` : n° de l'activité à gérer
        * ``validation`` : fin de l'url, peut être ``"/validate"``, ``"/invalidate"`` ou ``/delete``
       """
    validation, idact = kwargs["validation"], kwargs["idact"]
    variables_standard = kwargs["variables_standard"]
    if (request.method == "GET") and (validation == "/delete"):
        # suppression de l'activité effectuée par une fonction dédiée
        utilities._del_activite(sock, request, idact)
        return HttpResponseRedirect('{}activites/'.format(settings.NOTE_ROOT_URL))
    sock.write(json.dumps(["mayi", "activites_admin"]))
    hasadmin = nk.full_read(sock)["msg"]
    if not hasadmin:
        messages.add_error(request, _(u"Tu n'as pas le droit activites_admin !"))
        return HttpResponseRedirect("{}activites/".format(settings.NOTE_ROOT_URL))
    activite = utilities._get_activite(sock, idact, request, computecandelete=True, whoami=variables_standard["whoami"], isadmin=True)
    variables = {}
    variables["activite"] = activite
    variables["hasadmin"] = hasadmin
    if validation == "/validate":
        action, data, succmsg = "valider_activite", idact, messages.SUCCMSG_VALIDACT
    elif validation == "/invalidate":
        action, data, succmsg = "devalider_activite", idact, messages.SUCCMSG_DEVALIDACT
    elif validation in ["/open", "/close"]:
        action, data = "openclose_activite", [(validation == "/open"), idact]
        succmsg = messages.SUCCMSG_OUVRACT if data[0] else messages.SUCCMSG_FERMACT
    else:
        action = None
    if action:
        sock.write(json.dumps([action, data]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            if out["retcode"] == 0:
                messages.add_success(request, succmsg)
            else:
                messages.add_warning(request, out["errmsg"])
        else:
            messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect('{}activites/{}/gestion/'.format(settings.NOTE_ROOT_URL, idact))
    return (variables, "note/activite_gestion.html")

@standard_page
def activite_gestion_modifier(request, sock, kwargs):
    """Page pour voir/éditer une activité, en tant qu'admin

       Dans ``kwargs`` :
        * ``idact`` : n° de l'activité à modifier
        """

    sock.write(json.dumps(["mayi", "activites_admin"]))
    hasadmin = nk.full_read(sock)["msg"]
    if not hasadmin:
        messages.add_error(request, _(u"Tu n'as pas le droit activites_admin !"))
        return HttpResponseRedirect("{}activites/".format(settings.NOTE_ROOT_URL))

    idact = kwargs["idact"]
    variables = {}
    activite = utilities._get_activite(sock, idact, request)
    variables["activite"] = activite
    variables["hasadmin"] = hasadmin
    if request.method == "GET":
        form = forms.ActiviteForm(label_suffix=" :", initial=activite)
        variables["form"] = form
        return (variables, "note/activite_modifier.html")
    else:
        form = forms.ActiviteForm(request.POST, label_suffix=" :")
        variables["form"] = form
        if form.is_valid():
            keysact = activite.keys()
            # on regarde les champs qui sont différents
            actdata = {k: v for (k, v) in form.cleaned_data.items() if k in keysact and (v != activite[k])}
            if actdata != {}:
                # On rajoute l'idact
                actdata["id"] = idact
                # On demande toujours à faire l'update en tant qu'admin
                #  de toutes façon ça ne provoque pas d'erreur si on ne l'est pas
                tosend = [actdata, "A"]
                sock.write(json.dumps(["update_activite", tosend]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, messages.SUCCMSG_CHGACT)
                else:
                    messages.add_error(request, out["errmsg"])
                    return (variables, "note/activite_modifier.html")
            # on renvoie sur la page de visualisation de l'activité modifiée
            return HttpResponseRedirect("{}activites/{}/gestion/".format(settings.NOTE_ROOT_URL, idact))
        else:
            return (variables, "note/activite_modifier.html")

@standard_page_withignores(["idact"])
def mes_activites(request, sock, kwargs):
    """Page "Mes Activités" (ajout, modification, suppression si non encore validée)

       Dans ``kwargs`` :
        * ``idact`` si on modifie une activité
        * ``delete="/delete"`` si on supprime une activité
        """
    idact, delete = kwargs["idact"], kwargs["delete"]
    variables_standard = kwargs["variables_standard"]
    variables = {}
    # on demande si on le droit d'être admin
    sock.write(json.dumps(["mayi", "activites_admin"]))
    hasadmin = nk.full_read(sock)["msg"]
    variables["hasadmin"] = hasadmin
    if idact is None:
        mes_activites = utilities._get_activites(sock, False, False, request, computecandelete=True, whoami=variables_standard["whoami"], mine=True)
        variables["activites"] = mes_activites
        if request.method == "POST":
            form = forms.ActiviteForm(request.POST, label_suffix=" :", listeimprimee=False)
            if form.is_valid():
                actdata = form.cleaned_data
                del actdata["id"]
                sock.write(json.dumps(["add_activite", actdata]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, messages.SUCCMSG_ADDACT)
                    return HttpResponseRedirect('{}mes_activites/'.format(settings.NOTE_ROOT_URL))
                else:
                    messages.add_error(request, out["errmsg"])
        else:
            form = forms.ActiviteForm(label_suffix=" :", listeimprimee=False)
        variables["form"] = form
        return (variables, "note/mes_activites.html")
    else:
        if delete == "/delete":
            succeed = utilities._del_activite(sock, request, idact)
            return HttpResponseRedirect('{}mes_activites/'.format(settings.NOTE_ROOT_URL))
        activite = utilities._get_activite(sock, idact, request, fallback='{}mes_activites/'.format(settings.NOTE_ROOT_URL))
        variables["activite"] = activite
        if request.method == "GET":
            form = forms.ActiviteForm(label_suffix=" :", initial=activite, listeimprimee=False)
        else:
            form = forms.ActiviteForm(request.POST, label_suffix=" :", listeimprimee=False)
            if form.is_valid():
                actdata = {champ: valeur for (champ, valeur) in form.cleaned_data.items() if (champ == "id") or (valeur != activite[champ])}
                sock.write(json.dumps(["update_activite", [actdata, "A"]]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, messages.SUCCMSG_CHGACT)
                    return HttpResponseRedirect('{}mes_activites/'.format(settings.NOTE_ROOT_URL))
                else:
                    messages.add_error(request, out["errmsg"])
        variables["form"] = form
        return (variables, "note/activite_modifier.html")

@standard_page
def del_invite(request, sock, kwargs):
    """Suppression d'un invité

       Dans ``kwargs`` :
        * ``idact`` : l'activité dont on veut retirer l'invité
        * ``idinv`` : l'invité qu'on veut retirer
        * ``admin="/admin"`` si on est mode administration"""

    variables = {}
    idact, idinv = kwargs["idact"], kwargs["idinv"]
    admin = (kwargs["admin"] == "/admin")
    if idinv != -1:
        # On demande toujours la suppression avec le flag "A",
        # de toutes façons il est ignoré par le serveur si on n'a pas les droits
        sock.write(json.dumps(["del_invite", [idinv, "A"]]))
        out = nk.full_read(sock)
        if out["retcode"] == 404:
            messages.add_error(request, messages.ERRMSG_IDINV_FAIL.format(idinv))
        elif nk._is_success_code(out["retcode"]):
            messages.add_success(request, messages.SUCCMSG_DELINV)
        else:
            messages.add_error(request, out["errmsg"])
    return HttpResponseRedirect(u"{}activites/{}/{}".format(settings.NOTE_ROOT_URL, idact, "admin/" * admin))

@standard_page
def liste_invites_entres(request, sock, kwargs):
    """Affiche la liste des invités qui sont rentrés à l'activité

       Dans ``kwargs`` :
        * ``idact`` : l'activité dont on veut la liste des invités rentrés
    """

    variables = {}
    idact = kwargs["idact"]

    #On récupère l'ensemble des droits dont dispose l'utilisateur
    sock.write(json.dumps(["mayi", "full_rights"]))
    out = nk.full_read(sock)

    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}activites/".format(settings.NOTE_ROOT_URL))
    else:
        droits =  out["msg"]["droits"]
        hasadmin = "activites_admin" in droits

    if not hasadmin:
       messages.add_error(request, _(u"Tu n'as pas le droit activites_admin !"))
       return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))

    # On récupère la liste des invités qui sont rentrés à cette activité
    sock.write(json.dumps(["liste_invites_entres", [idact,]]))
    out = nk.full_read(sock)
    if out["retcode"] == 404:
        messages.add_error(request, messages.ERRMSG_IDACT_FAIL % (idact,))
        return HttpResponseRedirect("{}activites/".format(settings.NOTE_ROOT_URL))
    elif not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}activites/".format(settings.NOTE_ROOT_URL))

    else:
        invites_entres = out["msg"]
        # On prépare les variables
        variables = {"invites_entres": invites_entres,
                     "idact" : idact,
                     "hasadmin": hasadmin,
                     "nb_invites": len(invites_entres),
                     }


    return (variables, "note/invites_entres.html")


@standard_page_withignores(["idbde"])
def comptes(request, sock, kwargs):
    """La page de recherche d'un compte ou qui l'affiche.

       Dans ``kwargs`` :
        * ``idbde`` si on affiche un compte
        """
    idbde = kwargs["idbde"]
    if request.method == "GET":
        if idbde is None:
            return ({}, "note/comptes.html")
        else:
            variables = utilities._prepare_variables(sock, idbde, request)
            variables["active"] = "compte"
            return (variables, "note/un_compte.html")
    else:
        return HttpResponse("Méthode invalide", status=400)

@standard_page_withignores(["idbde", "idact"])
def readhesions(request, sock, kwargs):
    """La page de recherche d'un compte ou qui l'affiche.

       Dans ``kwargs`` :
        * ``idbde`` si on affiche un compte
        * ``idact`` si on vient d'une entrée pot
        """
    idbde, idact = kwargs["idbde"], kwargs["idact"]
    tarifs = utilities._get_tarifs_adhesion(sock)
    if request.method == "GET":
        if idbde is None:
            return ({}, "note/readhesions.html")
        else:
            variables = utilities._prepare_variables(sock, idbde, request)
            variables.update(tarifs)
            form = forms.ReadhesionForm(label_suffix=" :",initial={"pay_nom" : variables["compte"]["nom"],"pay_prenom" : variables["compte"]["prenom"]})
            variables["form"] = form
            return (variables, "note/une_readhesion.html")
    else:
        form = forms.ReadhesionForm(request.POST, label_suffix=" :")
        if form.is_valid():
            readhdata = form.cleaned_data
            # Il faut formater ça correctement pour l'envoyer au serveur NK
            pay = {"type": readhdata["type_de_paiement"], "montant" : readhdata["on_note"],
                    "nom": readhdata["pay_nom"], "prenom": readhdata["pay_prenom"], "banque": readhdata["pay_banque"]}
            # On vire les champs en question du dico
            for champ in ["on_note", "type_de_paiement", "pay_nom", "pay_prenom", "pay_banque"]:
                del readhdata[champ]
            # On cherche à savoir combien lui coûte l'adhésion
            tarif = tarifs["prix_adhesion"]
            pay["montant"] += tarif
            if pay["type"] != "none":
                readhdata["pay"] = pay
            readhdata["idbde"] = idbde
            sock.write(json.dumps(["readherer", readhdata]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, messages.SUCCMSG_READHESION)
            else:
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect("{}readhesions/{}/".format(settings.NOTE_ROOT_URL, idbde))
            # si on provient de la page d'entrée pot alors on y retourne
            if idact is not None:
                return HttpResponseRedirect("{}activites/{}/entree/".format(settings.NOTE_ROOT_URL, idact))
            # sinon on renvoie sur la page de réadhésion
            else:
                return HttpResponseRedirect("{}readhesions/".format(settings.NOTE_ROOT_URL ))

        else:
            variables = utilities._prepare_variables(sock, idbde, request)
            variables.update(tarifs)
            variables["form"] = form
            # Le formulaire de réadhésion n'est pas valide, on renvoie le formulaire incomplet
            return (variables, "note/une_readhesion.html")

@standard_page
def historique_transactions(request, sock, kwargs):
    """Page de l'historique des transactions d'un compte.

       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut l'historique des transactions.
        * ``num_page`` : numéro de la page de l'historique demandé
        """
    idbde = kwargs["idbde"]
    nb = 1000 # Arbitraire, nb de consos par page

    if idbde is None:
        return HttpResponseRedirect('{}comptes/'.format(settings.NOTE_ROOT_URL))
    elif "num_page" in kwargs:
        num_page = int(kwargs["num_page"])
    else:
        return HttpResponseRedirect('{}comptes/{}/historique/1/'.format(settings.NOTE_ROOT_URL, idbde))

    variables = utilities._prepare_variables(sock, idbde, request)
    sock.write(json.dumps(["historique_transactions", [idbde, num_page, nb]]))
    out = nk.full_read(sock)
    if nk._is_success_code(out["retcode"]):
        variables['historique'] = out["msg"]['historique']
        variables['nb_transactions'] = out["msg"]['nb_transactions']
        variables['nb_pages'] = out["msg"]['nb_pages']
        variables['num_page'] = out["msg"]['num_page']
    else:
        messages.add_error(request, out["errmsg"])
    if variables['num_page'] != num_page:
        # Au cas où le serveur aurait eu besoin de renuméroter,
        # si l'utilisateur demande un page < 1 ou > max
        return HttpResponseRedirect("{}comptes/{}/historique/{}/".format(settings.NOTE_ROOT_URL, idbde, variables["num_page"],))
    variables["active"] = "historique"
    return (variables, "note/un_compte_historique.html")

@standard_page
def comptes_advanced(request, sock, kwargs):
    """Page de recherche avancée"""
    variables = {}
    # On cherche si on a le droit full_search
    sock.write(json.dumps(["mayi", "full_search"]))
    mayi = nk.full_read(sock)["msg"]
    variables["acl_full_search"] = mayi
    # certains champs ne seront accessibles qu'aux utilisateurs ayant les droits full_search
    variables["full_search_fields"] = ["tel", "adresse", "pbsante"]
    if request.method == "GET":
        form = forms.SearchForm(label_suffix=" :")
        variables["form"] = form
        variables["search_flags"] = "cio" # par défaut, la recherche est case-insensitive et sur les comptes non à jour
        variables["exactfilter"] = "b" # par défaut, on matche sur le début du mot
    else:
        form = forms.SearchForm(request.POST, label_suffix=" :")
        checked_fields = [champ[4:] for (champ, valeur) in request.POST.items() if (champ[:4] == "box_") and (valeur == "on")]
        variables["checked_fields"] = checked_fields
        exactfilter = request.POST["exactfilter"]
        variables["exactfilter"] = exactfilter
        search_flags_dico = {"search_alias": "A",
                             "give_alias": "a",
                             "search_historique": "H",
                             "give_historique": "h",
                             "case_insensitive": "i",
                             "old_accounts": "o",
                             "conjunctive_search" : "c"}
        search_flags = "".join([search_flags_dico.get(champ, "") for champ in request.POST.keys()])
        if form.is_valid():
            # On récupère toutes les données sur lesquelles on doit chercher
            searching = {champ: form.cleaned_data[champ] for champ in checked_fields}
            searchfields = searching.keys()
            if "alias" in searchfields:
                search_flags += "A"
            if "historique" in searchfields:
                search_flags += "H"
            variables["search_flags"] = search_flags
            flags = exactfilter + search_flags
            tosend = [flags, searching]
            sock.write(json.dumps(["search", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                # les champs alias et historiques ont déjà été gentiments post-processés par le serveur
                # mais quand il n'y en a pas, on récupère null, pythonisé en None, et c'est moche
                liste = out["msg"]
                for i in range(len(liste)):
                    for champ in ["aliases", "historiques"]:
                        if liste[i].get(champ, "") == None:
                            liste[i][champ] = u""
                variables["search_result"] = liste
                variables["give_alias"] = ("a" in search_flags)
                variables["give_historique"] = ("h" in search_flags)
                return (variables, "note/recherche_avancee_resultats.html")
            else:
                messages.add_error(request, out["errmsg"])
                variables["form"] = form
        else:
            variables["form"] = form
    return (variables, "note/recherche_avancee.html")

@standard_page
def modifier_compte(request, sock, kwargs):
    """Page de modification de compte.

       Dans ``kwargs`` :
        * ``idbde`` : id du compte à modifier
        """
    idbde = kwargs["idbde"]
    variables_standard = kwargs["variables_standard"]
    variables = utilities._prepare_variables(sock, idbde, request, form=True, whoami=variables_standard["whoami"])
    # On vérifie si l'utilisateur tente de se modifier lui-même
    updating_myself = False
    if variables["whoami"]["idbde"] == idbde and "myself" in variables["whoami"]["full_rights"]:
        updating_myself = True
    variables["updating_myself"] = updating_myself
    if request.method == "GET":
        if variables.has_key("compte"):
            form = forms.CompteForm(initial=variables["compte"])
        else:
            return HttpResponseRedirect("{}comptes/".format(settings.NOTE_ROOT_URL))
    elif request.method == "POST":
        # on a envoyé un formulaire de modification du compte
        form = forms.CompteForm(request.POST)
        if form.is_valid():
            fields = form.cleaned_data
            gotcompte = variables.has_key("compte")
            if not gotcompte:
                return HttpResponseRedirect("{}comptes/".format(settings.NOTE_ROOT_URL))
            compte = variables["compte"]
            keyscompte = compte.keys()
            # on regarde les champs qui sont différents
            tosend = {k: v for (k, v) in form.cleaned_data.items() if k in keyscompte and (v != compte[k])}
            # les droits ça se gère un peu spécialement
            # avec des virgules et des cases à cocher
            for champ in ["droits", "surdroits"]:
                n = len(champ) + 1
                droitsousur = [case[n:] for (case, valeur) in request.POST.items() if case.startswith("{}_".format(champ)) and (valeur == "on")]
                liste = compte[champ]
                if set(droitsousur) != set(liste):
                    tosend[champ] = u",".join(droitsousur)
            if tosend != {}: # si il y a vraiment quelque chose à faire
                # on rajoute l'idbde
                tosend["idbde"] = idbde
                # si on n'a pas les droits wei (et que ce n'est pas notre compte)
                #  on ne tient pas compte du contenu de "numsecu" et "pbsante"
                if not variables["has_wei"]:
                    if tosend.has_key("pbsante"):
                        del tosend["pbsante"]
                sock.write(json.dumps(["update_compte", tosend]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, messages.SUCCMSG_ACCOUNT_CHANGED)
                else:
                    messages.add_error(request, out["errmsg"])
                    variables["form"] = form
                    return (variables, "note/modifier_compte.html")
            # si on provient de la page listedroits alors on y retourne
            if request.META["HTTP_REFERER"].endswith("listedroits"):
                return HttpResponseRedirect("{}listedroits/".format(settings.NOTE_ROOT_URL ))
            # sinon renvoie sur la page de visualisation du compte modifié, même si rien n'a été modifié
            else:
                return HttpResponseRedirect("{}comptes/{}/".format(settings.NOTE_ROOT_URL, idbde))
        else:
            variables["form"] = form
            return (variables, "note/modifier_compte.html")
    variables["form"] = form
    variables["active"] = "modifier"
    return (variables, "note/modifier_compte.html")

@standard_page
def supprimer_compte(request, sock, kwargs):
    """Page de confirmation de suppression de compte.

       Dans ``kwargs`` :
        * ``idbde`` : id du compte à supprimer
        """
    idbde = kwargs["idbde"]
    variables = {"button_class" : "btn-danger", "button_content" : u"Confirmer la suppression"}
    variables_standard = kwargs["variables_standard"]
    variables.update(utilities._prepare_variables(sock, idbde, request, form=True, whoami=variables_standard["whoami"]))
    if request.method == "GET":
        if variables.has_key("compte"):
            form = forms.DeleteCompteForm(initial=variables["compte"])
        else:
            return HttpResponseRedirect("{}comptes/".format(settings.NOTE_ROOT_URL))
    elif request.method == "POST":
        # on a envoyé un formulaire de suppression du compte
        form = forms.DeleteCompteForm(request.POST)
        if form.is_valid():
            fields = form.cleaned_data
            if not variables.has_key("compte"):
                return HttpResponseRedirect("{}comptes/".format(settings.NOTE_ROOT_URL))
            compte = variables["compte"]
            # on regarde les champs qui sont différents
            tosend = [idbde, fields["anonymiser"]]
            sock.write(json.dumps(["supprimer_compte", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, messages.SUCCMSG_ACCOUNT_DELETED)
                # on renvoie sur la page de visualisation du compte supprimé
                return HttpResponseRedirect("{}comptes/{}/".format(settings.NOTE_ROOT_URL, idbde))
            else:
                messages.add_error(request, out["errmsg"])
    variables["form"] = form
    variables["active"] = "supprimer"
    return (variables, "note/supprimer_compte.html")

@standard_page
def update_photo(request, sock, kwargs):
    """La page de modification des photos.

       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut modifier la photo
        """
    idbde = kwargs["idbde"]
    variables = {}
    variables = utilities._prepare_variables(sock, idbde, request)
    if request.method == "POST":
        form = forms.PhotoForm(request.POST, request.FILES, label_suffix=" :")
        if form.is_valid():
            photo = request.FILES["photo"]
            # On envoie la photo au serveur NK
            photodata = photo.read()
            photob64 = base64.b64encode(photodata)
            format = photo.name.rsplit('.',1)[-1]
            sock.write(json.dumps(["update_photo", [idbde, photob64, format]]))
            answer = nk.full_read(sock)
            if nk._is_success_code(answer["retcode"]):
                messages.add_success(request, messages.SUCCMSG_PHOTO_UPDATED)
                return HttpResponseRedirect('{}comptes/{}/'.format(settings.NOTE_ROOT_URL, idbde))
            else:
                messages.add_error(request, answer["errmsg"])
    else:
        form = forms.PhotoForm(label_suffix=" :")
    variables["form"] = form
    variables["active"] = "modifier photo"
    return (variables, "note/un_compte_photo.html")

@standard_page
def historique_pseudo(request, sock, kwargs):
    """La page de visualisation de l'historique des pseudos.

       Dans ``kwargs`` :
        * ``idbde`` : id du compte ont on veut l'historique des pseudos
       """
    idbde = kwargs["idbde"]
    variables = {}
    if request.method == "GET":
        compte = utilities._get_historique_pseudo(sock, idbde, request)
        variables["compte"] = compte
        variables["historique_pseudo"] = compte["historique_pseudo"]
        return (variables, "note/historique_pseudo.html")
    else:
        return HttpResponse("Bad request method : {}".format(request.method))

@standard_page
def search_historique_pseudo(request, sock, kwargs):
    """Page de recherche par ancien pseudo (même inactif)."""
    variables = {}
    if request.method == "GET":
        form = forms.SearchHistoriquePseudoForm(label_suffix=" :", initial={"exactfilter": "b"})
        variables["form"] = form
    else:
        form = forms.SearchHistoriquePseudoForm(request.POST, label_suffix=" :")
        if form.is_valid():
            tosend = [form.cleaned_data["historique"], form.cleaned_data["exactfilter"]]
            sock.write(json.dumps(["search_historique_pseudo", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                variables["is_displaying"] = True
                liste = out["msg"]
                variables["search_result"] = liste
            else:
                messages.add_error(request, out["errmsg"])
        else:
            variables["form"] = form
    return (variables, "note/search_historique_pseudo.html")

@standard_page
def aliases(request, sock, kwargs):
    """La page de gestion des aliases.

       Dans ``kwargs`` :
        * ``idbde`` : l'id du compte
        """
    idbde = kwargs["idbde"]
    variables = {}
    compte = utilities._get_aliases(sock, idbde, request)
    variables["compte"] = compte
    if request.method == "GET":
        form = forms.AliasForm(label_suffix=" :")
    else:
        form = forms.AliasForm(request.POST, label_suffix=" :")
        if form.is_valid():
            alias = form.cleaned_data["alias"]
            tosend = [idbde, alias]
            sock.write(json.dumps(["alias", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, messages.SUCCMSG_ALIAS_ADDED)
                return HttpResponseRedirect("{}comptes/{}/aliases/".format(settings.NOTE_ROOT_URL, idbde))
            else:
                messages.add_error(request, out["errmsg"])
    variables["form"] = form
    return (variables, "note/aliases.html")

@standard_page
def unalias(request, sock, kwargs):
    """Fonction appelée lorsqu'on veut supprimer un/des alias.

       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut supprimer un/des alias
        * ``delall="_all"`` si on veut supprimer tous les alias du compte (prioritaire)
        * ``idalias`` : id d'un alias à supprimer
        """
    variables = {}
    idbde, delall, idalias = kwargs["idbde"], kwargs["delall"], kwargs["idalias"]
    if request.method == "GET":
        delete_all = (delall == "_all")
        if delete_all:
            sock.write(json.dumps(["unalias", [idbde, True]]))
        else:
            try:
                idalias = int(idalias)
            except:
                messages.add_error(request, messages.ERRMSG_IDALIAS_INVALID.format(idalias))
                return HttpResponseRedirect("{}comptes/{}/aliases/".format(settings.NOTE_ROOT_URL, idbde))
            sock.write(json.dumps(["unalias", [idalias, False]]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            if delete_all:
                msg = messages.SUCCMSG_ALIAS_ALLDELETED
            else:
                msg = messages.SUCCMSG_ALIAS_DELETED
            messages.add_success(request, msg)
        else:
            messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}comptes/{}/aliases/".format(settings.NOTE_ROOT_URL, idbde))
    else:
        return HttpResponse("Bad request method : {}".format(request.method))

@sensitive_post_parameters("password")
@standard_page
def password(request, sock, kwargs):
    """Page de changement de mot de passe.

       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut changer le mot de passe
        """
    idbde = kwargs["idbde"]
    variables = {}
    if request.method == "GET":
        compte = utilities._get_compte(sock, idbde, request)
        variables["compte"] = compte
        if kwargs["variables_standard"]["whoami"]["idbde"] == idbde:
            # On peut changer son propre mot de passe
            variables["form"] = forms.PasswordForm(label_suffix=" :")
        elif idbde <= 0:
            messages.add_error(request, messages.ERRMSG_PASSWORD_NEGATIVE_IDBDE)
        else:
            # Il faut alors vérifier si il a le droit chgpass
            sock.write(json.dumps(["mayi", "chgpass"]))
            out = nk.full_read(sock)["msg"]
            if out == True:
                variables["form"] = forms.PasswordForm(label_suffix=" :")
            else:
                messages.add_error(request, messages.ERRMSG_NO_ACL_CHGPASS)
        return (variables, "note/password.html")
    else:
        form = forms.PasswordForm(request.POST, label_suffix=" :")
        if form.is_valid():
            newpass = form.cleaned_data["password"]
            tosend = [idbde, newpass]
            sock.write(json.dumps(["chgpass", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, messages.SUCCMSG_PASSWORD_CHANGED)
                return HttpResponseRedirect("{}comptes/{}/".format(settings.NOTE_ROOT_URL, idbde))
            else:
                messages.add_error(request, out["errmsg"])
                variables["form"] = form
                return (variables, "note/password.html")
        else:
            compte = utilities._get_compte(sock, idbde, request)
            variables["compte"] = compte
            variables["form"] = form
            return (variables, "note/password.html")


def _create_BoutonForm(sock, request, contenu=None, initial=None):
    """Un hack pour peupler au runtime
       les choices du champ destinataire d'un formulaire de création de bouton."""
    if initial == None:
        if contenu == None:
            form = forms.BoutonForm(label_suffix=" :")
        else:
            form = forms.BoutonForm(contenu, label_suffix=" :")
    else:
        form = forms.BoutonForm(label_suffix=" :", initial=initial)
    categories = utilities._get_boutons_categories(sock, request, True)
    form.fields['categorie'].choices = [[ca] * 2 for ca in categories]
    clubs = utilities._get_clubs(sock, request)
    form.fields['destinataire'].choices = [[cl["idbde"], cl["pseudo"]] for cl in clubs]
    return form

@standard_page
def boutons(request, sock, kwargs):
    """Page de gestion des boutons.

       Dans ``kwargs`` :
        * ``idbouton`` : id du bouton
        * ``delete="/delete"`` si on cherche à le supprimer
        """
    variables = {}
    idbouton = kwargs.get("idbouton")
    delete = kwargs.get("delete")
    if delete == "/delete":
        sock.write(json.dumps(["delete_bouton", idbouton]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            messages.add_success(request, messages.SUCCMSG_DELBUTTON)
        else:
            messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect('{}boutons/'.format(settings.NOTE_ROOT_URL))
    if request.method == "GET":
        if idbouton == None:
            boutons = utilities._get_boutons(sock, request, hidden=True)
            variables["boutons"] = boutons
            # le formulaire est vide
            form = _create_BoutonForm(sock, request)
        else:
            bouton = utilities._get_un_bouton(sock, idbouton, request, fallback='{}boutons/'.format(settings.NOTE_ROOT_URL))
            variables["un_bouton"] = bouton
            # le formulaire contient les données du bouton
            form = _create_BoutonForm(sock, request, initial=bouton)
        variables["form"] = form
    else:
        form = _create_BoutonForm(sock, request, contenu=request.POST)
        if form.is_valid():
            if idbouton == None: # c'est donc un ajout
                sock.write(json.dumps(["create_bouton", form.cleaned_data]))
            else: # c'est une modification
                data = form.cleaned_data
                data["id"] = idbouton
                sock.write(json.dumps(["update_bouton", data]))
            out = nk.full_read(sock)
            retcode = out["retcode"]
            if retcode == 103:
                # C'est le code "Ce bouton existe déjà, c'est pas la peine de l'ajouter"
                messages.add_success(request, out["errmsg"])
                return HttpResponseRedirect("{}boutons/".format(settings.NOTE_ROOT_URL))
            elif nk._is_success_code(retcode):
                if idbouton == None: #ajout
                    messages.add_success(request, messages.SUCCMSG_ADDBUTTON)
                else: #modification
                    messages.add_success(request, messages.SUCCMSG_CHGBUTTON)
                return HttpResponseRedirect("{}boutons/".format(settings.NOTE_ROOT_URL))
            else:
                messages.add_error(request, out["errmsg"])
        else:
            if idbouton == None:
                boutons = utilities._get_boutons(sock, request)
                variables["boutons"] = boutons
            else:
                # on est dans le cas : je poste pour modifier un bouton, mais j'ai échoué
                # on a besoin de la présence de la variable un_bouton mais on n'a pas besoin d'aller le chercher
                # on a juste besoin de son id
                variables["un_bouton"] = {"id": idbouton}
            variables["form"] = form
    return (variables, "note/boutons.html")

@standard_page
def preinscriptions(request, sock, kwargs):
    """Page de gestion des préinscriptions."""
    variables = {}
    if request.method == "GET":
        form = forms.PreinscriptionForm(label_suffix=" :")
    else:
        form = forms.PreinscriptionForm(request.POST, label_suffix=" :")
        if form.is_valid():
            preinsdata = form.cleaned_data
            sock.write(json.dumps(["preinscrire", preinsdata]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, messages.SUCCMSG_PREINSCRIPTION_ADDED)
                # on ne fait pas un redirect, mais on vide le formulaire
                form = forms.PreinscriptionForm(label_suffix=" :")
            else:
                messages.add_error(request, out["errmsg"])
    variables["form"] = form
    return (variables, "note/preinscriptions.html")

@standard_page_withignores(["preid"])
def inscriptions(request, sock, kwargs):
    """Page de gestion des inscriptions.

       Dans ``kwargs`` :
        * ``preid`` : preid de la préinscription dont on s'occupe, vaut idwei si on vient du wei
        * ``delete="/delete"`` si on cherche à la supprimer
        * ``fromwei="/from_wei"` si on a une inscription via le wei
        """
    variables = {}
    preid, delete, fromwei = kwargs["preid"], kwargs["delete"], kwargs["fromwei"]
    from_wei = (fromwei == "/from_wei")
    if preid != None:
        # Si on est face à une inscription classique, on récupère la préinscription
        if not from_wei:
            preins = utilities._get_preinscription(sock, preid, request)
        # Sinon, on récupère la préinscription du wei
        if from_wei:
            preins = utilities._get_wei(sock, preid, request)
        if delete == "/delete":
            sock.write(json.dumps(["del_preinscription", preid]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, messages.SUCCMSG_PREINSCRIPTION_DELETED)
            else:
                messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect('{}inscriptions/'.format(settings.NOTE_ROOT_URL))
        variables["preinscription"] = preins
        full_rights, _ = utilities._get_full_rights(sock)
        tarifs = utilities._get_tarifs_adhesion(sock)
        variables["tarifs"] = tarifs
        # Si on a une inscription wei qu'on paye par la SoGe, le tarif vaut 0
        if from_wei and (preins["soge"] == True):
            variables["tarifs"] = 0
        if request.method == "POST":
            form = forms.InscriptionForm(request.POST, label_suffix=" :", full_rights=full_rights)
            if form.is_valid():
                inscription = form.cleaned_data
                soge = inscription["type_de_paiement"]=="soge"
                # override_adh ne fait pas partie des données du compte
                override_adh = int(round(100 * inscription["override_adh"]))
                del inscription["override_adh"]
                # Il faut formater ça correctement pour l'envoyer au serveur NK
                pay = [inscription["on_note"], inscription["type_de_paiement"],
                        {"nom": inscription["pay_nom"]+soge*inscription["nom"], "prenom": inscription["pay_prenom"]+soge*inscription["prenom"], "banque": inscription["pay_banque"]}]
                # On vire les champs en question du dico
                for champ in ["on_note", "type_de_paiement", "pay_nom", "pay_prenom", "pay_banque"]:
                    del inscription[champ]
                tosend = [preid, inscription, pay, from_wei, override_adh]
                sock.write(json.dumps(["inscrire", tosend]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    # L'inscription a fonctionné
                    messages.add_success(request, messages.SUCCMSG_ACCOUNT_ADDED)
                    # On redirige vers la page pertinente, selon que l'on vient du wei ou pas
                    if from_wei:
                        return HttpResponseRedirect('{}wei/inscrits1a/'.format(settings.NOTE_ROOT_URL))
                    else:
                        return HttpResponseRedirect('{}inscriptions/'.format(settings.NOTE_ROOT_URL))
                else:
                    messages.add_error(request, out["errmsg"])
        else:
            # Si on vient du wei, on récupère le pseudo de la preinscription
            if from_wei:
                preins["pseudo"] = preins["note"]
            else:
                # on rajoute dans le champ pseudo la valeur par défaut
                sock.write(json.dumps(["get_default_pseudo", [preins["nom"], preins["prenom"]]]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    preins["pseudo"] = out["msg"]
                else:
                    messages.add_error(request, out["errmsg"])
            initial = preins
            # Par défaut on remplit ce champ avec le prix de l'adhésion sans WEI
            # Le JS s'occupera de toutes façons de régler ça
            # car peut-être qu'à cette période de l'année, on a WEI coché par défaut
            initial["override_adh"] = tarifs["prix_adhesion"] / 100
            form = forms.InscriptionForm(label_suffix=" :", initial=initial, full_rights=full_rights)
        variables["form"] = form
    else:
        liste_preinscriptions = utilities._get_preinscriptions(sock, request)
        variables["liste_preinscriptions"] = liste_preinscriptions
    return (variables, "note/inscriptions.html")

@standard_page
def WEIaccueil(request, sock, kwargs):
    """
        Page d'accueil pour les inscriptions au WEI 2019.
    """
    variables = {}

    # On récupère les infos de base de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}".format(settings.NOTE_ROOT_URL))

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # JSON ne peut pas nativement transmettre un datetime : On transforme donc la chaîne en date
    to_date = lambda x: datetime(*map(int, x.split('-')))

    variables["wei_begin"] = to_date(variables["wei_begin"])
    variables["wei_end"] = to_date(variables["wei_end"])

    return (variables, "note/wei_accueil.html")

@standard_page
def WEIinscrits1A(request, sock, kwargs):
    """
        Liste des inscrits au WEI en 1A
    """
    variables = {}

    # On récupère les infos de base de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    haswei = out["msg"]["haswei"]

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    if not haswei:
        messages.add_error(request, "Vous ne pouvez pas accéder à la liste des inscrits/préinscrits !")

    else:
        # On cherche dans la base de données d'inscription des 1A
        champs = ['nom', 'prenom', 'tel', 'mail', 'urgence_nom', 'urgence_tel', 'dept', 'soge',
            'normalien', 'note', 'infos',]

        sock.write(json.dumps(["wei_get_listes", [True,]]))
        out = nk.full_read(sock)

        if out["retcode"] != 0:
            messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

        variables.update(out["msg"])

    return (variables, "note/wei_inscrits1a.html")

@standard_page
def WEIcreerCompte1A(request, sock, kwargs):
    """
        Crée un compte Note Kfet aux nouveaux 1A
    """
    variables = {}

    # On récupère les infos de base de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)


    haswei = out["msg"]["haswei"]

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    if not haswei:
        messages.add_error(request, "Vous ne pouvez pas adhérer les 1A !")
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    idwei = int(kwargs['idwei'])

    sock.write(json.dumps(['wei_creer_note', idwei]))
    out = nk.full_read(sock)

    if out["msg"] == True:
        messages.add_success(request, "Nouvelle note créée !")
    else:
        messages.add_error(request, u"Echec lors de la création de la nouvelle note: \n{}".format(out["errmsg"]))

    return HttpResponseRedirect("{}wei/inscrits1a/".format(settings.NOTE_ROOT_URL))

@standard_page
def WEIinscrits(request, sock, kwargs):
    """
       Liste des inscrits au WEI
    """
    variables = {}

    # On récupère les infos de base de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    haswei = out["msg"]["haswei"]

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # On vérifie que l'utilisateur a le droit de voir la page
    if not haswei:
        messages.add_error(request, "Vous ne pouvez pas accéder à la liste des inscrits/préinscrits !")
    else:
        if request.method == 'POST':
            # Les infos recueillies dans le POST sont envoyées pour mettre à jour la table
            sock.write(json.dumps(["wei_update_tables", [ request.POST.dict(),], ]))
            out = nk.full_read(sock)
            #Le serveur répond quant à la validité des modifications
            if out["retcode"] != 0:
                messages.add_error(request, out["errmsg"])
            else:
                messages.add_success(request, "Modifications effectuées avec succès")

        # Dans tous les cas, si l'utilisateur a le droit, on récupère
        # la liste des inscriptions/preinscriptions
        sock.write(json.dumps(["wei_get_listes", [False,]]))
        out = nk.full_read(sock)
        if out["retcode"] != 0:
            messages.add_error(request, out["errmsg"])

        variables["liste_pre"] = out["msg"]["liste_pre"]
        variables["liste_insc"] = out["msg"]["liste_insc"]
        variables["nb_pre_total"] = out["msg"]["nb_pre"][0]
        variables["nb_pre_paye"] = out["msg"]["nb_pre"][1]
        variables["nb_pre_caution"] = out["msg"]["nb_pre"][2]
        variables["nb_insc"] = out["msg"]["nb_insc"]

    return (variables, "note/wei_inscrits.html")

@standard_page
def WEIreadherer(request, sock, kwargs):
    """
        Effectue la réadhésion d'un (pré)inscrit au WEI en 2A+
    """
    variables = {}

    # On récupère les infos de base de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    haswei = out["msg"]["haswei"]

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # On vérifie que l'utilisateur a le droit de voir la page
    if not haswei:
        messages.add_error(request, "Vous ne pouvez pas réadhérer un inscrit/préinscrit !")
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    sock.write(json.dumps(["wei_readherer", int(kwargs.get('idwei'))]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
    else:
        messages.add_success(request, "Réadhésion effectuée avec succès")

    return HttpResponseRedirect("{}wei/inscrits/".format(settings.NOTE_ROOT_URL))

@standard_page
def WEIchangeInscription(request, sock, kwargs):
    """
        Page destinée aux respos WEI et respos info pour modifier les infos
        concernant l'inscription WEI d'un membre.
    """
    variables = {}

    # On demande au backend quels sont les droits de
    # l'utilisateur concernant l'application WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # On récupère l'idwei que l'on veut modifier
    # et les infos de l'inscription
    try:
        idwei = int(kwargs.get("idwei"))
    except:
        messages.add_error(request, "L'id WEI n'est pas correct")
        return HttpResponseRedirect("{}wei/inscrits".format(settings.NOTE_ROOT_URL))

    this_compte = kwargs["variables_standard"]["whoami"]
    if request.method == 'GET':
        sock.write(json.dumps(["wei_search", [
                                                 {
                                                     'is_pre' : False,
                                                     'id' : this_compte["idbde"],
                                                     'champs' : { 'idwei' : idwei },
                                                 },
                                             ]
                              ]))

        out = nk.full_read(sock)

        if out["retcode"] != 0:
            messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

        initial_data = out["msg"]["resultats"][0]
        # /!\ On doit séparer les différents choix pour les rôles
        # afin de pouvoir les mettre dans le formulaire
        initial_data["role"] = initial_data["role"].split(';')

        form = forms.WEIVieuxChangeForm(initial=initial_data)
        variables["form"] = form

        # On récupère le compte que l'inscription concerne
        variables["compte"] = utilities._get_compte(sock, initial_data["idbde"], request)

        return (variables, "note/wei_changeInscription.html")

    elif request.method == 'POST':
        form = forms.WEIVieuxForm(request.POST)
        if form.is_valid():
            sock.write(json.dumps(["wei_modify", [idwei, form.cleaned_data, ], ]))
            out = nk.full_read(sock)
            if out["retcode"] != 0:
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect("{}wei/inscrits/%d".format(settings.NOTE_ROOT_URL, idwei, ))

            messages.add_success(request, "Modifications effectuées avec succès")
            return HttpResponseRedirect("{}wei/inscrits".format(settings.NOTE_ROOT_URL))
        else:
            messages.add_error(request, "Le formulaire est invalide")
            variables["form"] = form
            return (variables, "note/wei_changeInscription.html")
    else:
        messages.add_error(request, request.method + " non supporté")
        return HttpResponseRedirect("{}wei/inscrits".format(settings.NOTE_ROOT_URL))

@standard_page
def WEIchangeInscription1A(request, sock, kwargs):
    """
        Page destinée aux respos WEI et respos info pour modifier les infos
        concernant l'inscription WEI d'un membre.
    """
    variables = {}

    # On demande au backend quels sont les droits de
    # l'utilisateur concernant l'application WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # On récupère l'idwei que l'on veut modifier
    # et les infos de l'inscription
    try:
        idwei = int(kwargs.get("idwei"))
    except:
        messages.add_error(request, "L'id WEI n'est pas correct")
        return HttpResponseRedirect("{}wei/inscrits".format(settings.NOTE_ROOT_URL))

    if request.method == 'GET':
        sock.write(json.dumps(["wei_search", [
                                                 {
                                                     'is_pre' : True,
                                                     'id' : None,
                                                     'champs' : { 'idwei' : idwei },
                                                 },
                                             ]
                              ]))

        out = nk.full_read(sock)

        if out["retcode"] != 0:
            messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

        # On récupère le résultat de la requête.
        # ie, les réponses aux questions et les mots
        initial_data = out["msg"]["resultats"][0]
        # /!\ On doit séparer les différents choix pour les rôles
        # afin de pouvoir les mettre dans le formulaire
        #initial_data["q_soirees"] = initial_data["q_soirees"].split(';')
        #initial_data["q_alcools"] = initial_data["q_alcools"].split(';')
        #initial_data["q_encadrement"] = initial_data["q_encadrement"].split(';')
        #initial_data["q_sociable"] = initial_data["q_sociable"].split(';')
        #initial_data["q_chants"] = initial_data["q_chants"].split(';')
        #initial_data["q_boire"] = initial_data["q_boire"].split(';')
        #initial_data["q_assos"] = initial_data["q_assos"].split(';')
        #initial_data["q_suiveur"] = initial_data["q_suiveur"].split(';')
        #initial_data["q_activites"] = initial_data["q_activites"].split(';')
        #initial_data["q_conquetes"] = initial_data["q_conquetes"].split(';')

        form = forms.WEI1AForm(initial=initial_data, label_suffix=_(u" :"))
        variables["form"] = form

        return (variables, "note/wei_changeInscription.html")

    elif request.method == 'POST':
        form = forms.WEI1AForm(request.POST)
        if form.is_valid():
            sock.write(json.dumps(["wei_modify1a", [idwei, form.cleaned_data, ], ], default=utilities.json_serialize))
            out = nk.full_read(sock)
            if out["retcode"] != 0:
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect("{}wei/inscrits1a/{}".format(settings.NOTE_ROOT_URL, idwei, ))

            messages.add_success(request, "Modifications effectuées avec succès")
            return HttpResponseRedirect("{}wei/inscrits1a".format(settings.NOTE_ROOT_URL))
        else:
            messages.add_error(request, "Le formulaire est invalide")
            variables["form"] = form
            return (variables, "note/wei_changeInscription.html")
    else:
        messages.add_error(request, request.method + " non supporté")
        return HttpResponseRedirect("{}wei/inscrits1a".format(settings.NOTE_ROOT_URL))

@standard_page
def WEImonInscription(request, sock, kwargs):
    """
        Page d'informations sur l'inscription de l'utilisateur.
    """
    variables = {}

    # On récupère les informations du compte pour le moteur
    # de template
    compte = kwargs["variables_standard"]["whoami"]
    variables["compte"] = compte

    # On demande au backend quels sont les droits de
    # l'utilisateur concernant l'application WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    myself = out["msg"]["myself"]

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # On vérifie que le compte a le droit de modifier ses propres infos
    # et qu'il s'agit bien d'une personne
    if not myself or compte.get("type","special") != "personne":
        messages.add_error(request, "Ce compte n'a pas la possibilité de s'inscrire au WEI")
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    # Si c'est le cas, on recherche une éventuelle (pré)inscription à son nom
    sock.write(json.dumps(["wei_search", [
                                             {
                                                 'is_pre' : False,
                                                 'id' : compte["idbde"],
                                                 'champs' : { 'idbde' : compte["idbde"] },
                                             },
                                         ]
                          ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    done = out["msg"]["done"]

    variables["done"] = done
    variables["etat_demande"] = out["msg"]["etat_demande"]

    # Si la demande n'a pas encore été effectuée...
    if not done:
        if request.method == 'GET':
            # On renvoie un formulaire
            dept = utilities._get_dept_code(compte["section"])
            compte["dept"] = dept
            form = forms.WEImonInscriptionForm(initial=compte)
        elif request.method == 'POST':
            # On traite la demande d'inscription
            form = forms.WEImonInscriptionForm(request.POST)
            if form.is_valid():
                form.cleaned_data["idbde"] = compte["idbde"]
                form.cleaned_data["prenom"] = compte["prenom"]
                form.cleaned_data["nom"] = compte["nom"]
                form.cleaned_data["mail"] = compte["mail"]
                form.cleaned_data["pseudo"] = compte["pseudo"]
                sock.write(json.dumps(["wei_compute_form", ["vieux", form.cleaned_data, ], ]))
                out = nk.full_read(sock)
                if out["retcode"] != 0:
                    messages.add_error(request, out["errmsg"])
                    variables["succes"] = False
                else:
                    variables["succes"] = True

                return (variables, "note/wei_succes.html")
        else:
            messages.add_error(request, "Requête non supportée : {}".format(request.method))
            return HttpResponseRedirect("{}wei/monInscription".format(settings.NOTE_ROOT_URL))

        variables["form"] = form

    elif done == "vieux":
        # La demande d'inscription a déjà été faite
        # On affiche à l'utilisateur les infos sur son inscription
        variables["type"] = "vieux"
        variables["infos"] = out["msg"]["etat_demande"]

    elif done == "1a":
        # Il s'agit d'un 1A, pour le moment on redirige vers la page d'accueil WEI
        variables["type"] = "1a"
        variables["infos"] = out["msg"]["etat_demande"]

    return (variables, "note/wei_monInscription.html")

@standard_page
def WEI1A(request, sock, kwargs):
    """
        Formulaire WEI pour les 1A.
    """
    variables = {}

    # Remets à zéro les réponses du nouveau formulaire
    request.session["wei1A2019"] = {}
    request.session["quest"] = ""

    # On récupère les paramètres habituels de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei".format(settings.NOTE_ROOT_URL))

    preinscriptions = out["msg"]["preinscriptions"]

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # Si le compte ne peux pas effectuer de préinscriptions, l'accès
    # est refusé
    if not preinscriptions:
        messages.add_error(request, "Tu ne peux pas effectuer de préinscriptions.")
        return HttpResponseRedirect("%wei/".format(settings.NOTE_ROOT_URL))
    else:

        if request.method == "GET":
            variables["form"] = forms.WEI1AForm(label_suffix=_(u" :"))
            variables["type"] = '1a'
        elif request.method == "POST":
            form = forms.WEI1AForm(request.POST)

            if form.is_valid():
                request.session["wei1A2019"]["nom"] = form.cleaned_data["nom"]
                request.session["wei1A2019"]["prenom"] = form.cleaned_data["prenom"]
                request.session["wei1A2019"]["date_naissance"] = json.dumps(
                    form.cleaned_data["date_naissance"],
                    default=utilities.json_serialize)
                request.session["wei1A2019"]["genre"] = form.cleaned_data["genre"]
                request.session["wei1A2019"]["adresse"] = form.cleaned_data["adresse"]
                request.session["wei1A2019"]["mail"] = form.cleaned_data["mail"]
                request.session["wei1A2019"]["ml_evenements"] = 't' if form.cleaned_data["ml_evenements"] else 'f'
                request.session["wei1A2019"]["normalien"] = 't' if form.cleaned_data["normalien"] else 'f'
                request.session["wei1A2019"]["tel"] = form.cleaned_data["tel"]
                request.session["wei1A2019"]["urgence_nom"] = form.cleaned_data["urgence_nom"]
                request.session["wei1A2019"]["urgence_tel"] = form.cleaned_data["urgence_tel"]
                request.session["wei1A2019"]["dept"] = form.cleaned_data["dept"]
                request.session["wei1A2019"]["note"] = form.cleaned_data["note"]
                request.session["wei1A2019"]["infos"] = form.cleaned_data["infos"]
                request.session["wei1A2019"]["soge"] = 't' if form.cleaned_data["soge"] else 'f'
                request.session["wei1A2019"]["etranger"] = 't' if form.cleaned_data["etranger"] else 'f'
                request.session["wei1A2019"]["ml_bds"] = 't' if form.cleaned_data["ml_bds"] else 'f'
                request.session["wei1A2019"]["ml_bda"] = 't' if form.cleaned_data["ml_bda"] else 'f'
                request.session["wei1A2019"]["petit_mot"] = form.cleaned_data["petit_mot"]
                request.session["nb_quest"] = 0

                if out["retcode"] != 0:
                    messages.add_error(request, out["errmsg"])
                    variables["succes"] = False
                    return (variables, "note/wei_1a.html")
                else:
                    variables["succes"] = True
                    variables["liste"] = algo1a.test(True, [], request.session["wei1A2019"]["nom"] + request.session["wei1A2019"]["prenom"])
                    variables["wei"] = request.session["wei1A2019"]
                    variables["nb_quest"] = 0
                    return (variables, "note/page_form.html")

            else:
                variables["form"] = form
                variables["questionnaire"] = formset_factory(forms.QuestionForm, extra=2)
                variables["type"] = '1a'
                pass
        else:
            messages.add_error(request, "Requête inattendue : {}".format(request.method))
            return HttpResponseRedirect("{}wei/1a/".format(settings.NOTE_ROOT_URL))

    return (variables, "note/wei_1a.html")

@standard_page
def WEI1Aalgo(request, sock, kwargs):
    """
        Formulaire WEI pour les 1A.
    """

    variables = {}

    # On récupère les paramètres habituels de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}wei".format(settings.NOTE_ROOT_URL))

    preinscriptions = out["msg"]["preinscriptions"]

    variables.update(out["msg"])
    variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # Si le compte ne peux pas effectuer de préinscriptions, l'accès
    # est refusé
    if not preinscriptions:
        messages.add_error(request, "Tu ne peux pas effectuer de préinscriptions.")
        return HttpResponseRedirect("%wei/".format(settings.NOTE_ROOT_URL))
    else:

        if request.method == "GET":

	    if request.GET.get("action") == "return":
		repq = request.session["quest"].reverse()
		def lambada(x):
			for i in range(len(x)):
				if x[i] == '#':
					return x[i+1:].reverse()
 			return ''
		request.session["quest"] = lambada(repq)
            variables["form"] = forms.WEI1AForm(label_suffix=_(u" :"))
            variables["type"] = '1a'
            if not request.session["wei1A2019"].has_key("nom"):
                return redirect('/note/wei/1a')
            variables["liste"] = algo1a.test(True, [], request.session["wei1A2019"]["nom"] + request.session["wei1A2019"]["prenom"])


        elif request.method == "POST":
            separator = "#" if request.session["quest"] != "" else ""
            request.session["quest"] += separator + request.POST.get("reponse")
            variables["liste"] = algo1a.test(True, request.session["quest"].split("#"), request.session["wei1A2019"]["nom"] + request.session["wei1A2019"]["prenom"])
            request.session["nb_quest"] += 1
            variables["nb_quest"] = ((request.session["quest"].count("#")+1) * 100) / 20

            if variables["liste"] is None:  # Fin du questionnaire
                request.session["wei1A2019"]["quest"] = request.session["quest"]
                sock.write(json.dumps(["wei_compute_form", ["1a", request.session["wei1A2019"], ], ]))
                out = nk.full_read(sock)

                if out["retcode"] != 0:
                    messages.add_error(request, out["errmsg"])
                    variables["succes"] = False
                    return (variables, "note/wei_succes.html")
                else:
                    variables["succes"] = True
                    return (variables, "note/wei_succes.html")

        else:
            messages.add_error(request, "Requête inattendue : {}".format(request.method))
            return HttpResponseRedirect("{}wei/1a/".format(settings.NOTE_ROOT_URL))

    return (variables, "note/page_form.html")

@standard_page
def WEIvieux(request, sock, kwargs):
    """
        Affiche le formulaire WEI pour les vieux.
    """
    variables = {}

    compte = kwargs["variables_standard"]["whoami"]
    variables["comptes"] = compte

    # Récupération des paramètres de base de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, "Erreur lors de la récupération des données de wei_main")
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))
    else:
        variables.update(out["msg"])
        variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # On vérifie que l'utilisateur a le droit inscriptions
    if not out["msg"]["inscriptions"]:
        messages.add_error(request, "Tu n'as pas le droit d'enregistrer une inscription 2A+")
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    # Si on demande un formulaire d'inscription, on en renvoie un
    if request.method == "GET":
        variables["form"] = forms.WEIVieuxForm()
        variables["type"] = 'vieux'

    # Si on en soumet un, on le traite
    elif request.method == "POST":
        form = forms.WEIVieuxForm(request.POST)
        if form.is_valid():
            sock.write(json.dumps(["wei_compute_form", [ "vieux", form.cleaned_data, ], ]))
            out = nk.full_read(sock)

            if out["retcode"] != 0:
                messages.add_error(request, out["errmsg"])
                variables["succes"] = False
            else:
                variables["succes"] = True

            return (variables, "note/wei_succes.html")
        else:
            variables["form"] = form
            variables["type"] = 'vieux'

    else:
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))

    return (variables, "note/wei_vieux.html")

@standard_page
def WEIAdmin(request, sock, kwargs):
    """
        Page d'administration des données du WEI
    """
    variables = {}

    # Récupération des paramètres de base de l'appli WEI
    sock.write(json.dumps(["wei_main", ]))
    out = nk.full_read(sock)

    if out["retcode"] != 0:
        messages.add_error(request, "Erreur lors de la récupération des données de wei_main")
        return HttpResponseRedirect("{}wei/".format(settings.NOTE_ROOT_URL))
    else:
        variables.update(out["msg"])
        variables["is_personne"] = kwargs["variables_standard"]["whoami"].get("type", "special") == "personne"

    # Pour la transformation des str en dates
    to_date = lambda x: datetime(*map(int, x.split('-')))

    if request.method == "POST":
        # Traitement des données du formulaire
        form = forms.WEIAdminForm(request.POST)
        if form.is_valid():
            # Si le formulaire est valide, on met à jour les données de l'app
            sock.write(json.dumps(['wei_admin', form.cleaned_data ]))
            out = nk.full_read(sock)
            if out["retcode"] != 0:
                messages.add_error(request, "Impossible de modifier les paramètres de l'application WEI")
                return HttpResponseRedirect("{}wei/admin/".format(settings.NOTE_ROOT_URL))
            # Il faut remettre à jour les données de l'application
            sock.write(json.dumps(["wei_main", ]))
            out = nk.full_read(sock)
            if out["retcode"] != 0:
                messages.add_error(request, "Erreur lors de la récupération des nouveaux paramètres")
                return HttpResponseRedirect("%wei/admin/".format(settings.NOTE_ROOT_URL))
            messages.add_success(request, "Modification effectuée avec succès")
            variables.update(out["msg"])
            initial = {
                'wei_name' : variables['wei_name'],
                'wei_begin' : to_date(variables['wei_begin']),
                'wei_end' : to_date(variables['wei_end']),
                'wei_1a_available' : variables['wei_1a_available'],
                'wei_vieux_available' : variables['wei_vieux_available'],
                'wei_contact' : variables['wei_contact'],
                'prix_wei_normalien' : variables['prix_wei_normalien']/100.0,
                'prix_wei_non_normalien' : variables['prix_wei_non_normalien']/100.0,
            }
            form = forms.WEIAdminForm(initial=initial)

        variables["form"] = form
    else:
        initial = {
            'wei_name' : variables['wei_name'],
            'wei_begin' : to_date(variables['wei_begin']),
            'wei_end' : to_date(variables['wei_end']),
            'wei_1a_available' : variables['wei_1a_available'],
            'wei_vieux_available' : variables['wei_vieux_available'],
            'wei_contact' : variables['wei_contact'],
            'prix_wei_normalien' : variables['prix_wei_normalien']/100.0,
            'prix_wei_non_normalien' : variables['prix_wei_non_normalien']/100.0,
        }
        form = forms.WEIAdminForm(initial=initial)
        variables["form"] = form

    return (variables, "note/wei_admin.html")

@standard_page
def TresorerieAccueil(request,sock,kwargs):
    """ Affiche la page d'accueil de l'interface trésorerie """
    variables = {"page" : {}}

    #On vérifie qu'on a bien le droits ``tresorerie``
    sock.write(json.dumps(["mayi","tresorerie"]))
    out = nk.full_read(sock)
    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        variables["tresorerie"] = False
        return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))
    else:
        variables["tresorerie"] = out["msg"]

    if variables["tresorerie"] == False:
        messages.add_error(request, "Tu n'as pas le droit tresorerie.")
        return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))
    variables["page"]["accueil"] = True
    return (variables, "note/trez_accueil.html")

@standard_page
def TresorerieRemises(request,sock,kwargs):
    """ Affiche la page des 'remises' pour les trésoriers """
    variables = {"page" : {}}

    #On vérifie qu'on a bien le droits ``tresorerie``
    sock.write(json.dumps(["mayi","tresorerie"]))
    out = nk.full_read(sock)
    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        variables["tresorerie"] = False
    else:
        variables["tresorerie"] = out["msg"]

    if request.method == 'POST' :
        sock.write(json.dumps(["creer_remise","chèques"]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            messages.add_success(request, messages.SUCCMSG_REMISE_ADDED)
        else:
            messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect('{}index/'.format(settings.NOTE_ROOT_URL))

    sock.write(json.dumps(["liste_remises",]))
    out = nk.full_read(sock)
    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))

    variables["listes_remises"] = out["msg"]["listes_remises"]
    variables["page"]["remise"] = True
    return(variables, 'note/trez_remises.html')

@standard_page
def TresorerieCheques(request,sock,kwargs):
    """ Affiche la page 'chèques' pour les trésoreriers
        """
    variables = {"page" : {}}

    #On vérifie qu'on a bien le droits ``tresorerie``
    sock.write(json.dumps(["mayi","tresorerie"]))
    out = nk.full_read(sock)
    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        variables["tresorerie"] = False
    else:
        variables["tresorerie"] = out["msg"]

    # Si on affiche la page pour la première fois
    if request.method == 'GET':
        sock.write(json.dumps(["liste_cheques",["date"," "," "]]))
        out = nk.full_read(sock)
        if not nk._is_success_code(out["retcode"]):
            messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))

        initial_data = out["msg"]["dico"]
        variables["list"] = out["msg"]["list"]
        form = forms.GenericDateTreasury(initial=initial_data)
        variables["form"] = form
    # Si on renvoie la page avec un formulaire rempli
    elif request.method == 'POST':
        # request.POST renvoie ce que contient les fields du forms
        form = forms.GenericDateTreasury(request.POST)
        if form.is_valid():
            #TODO mettre les dates dans un dico pour être traiter côte serveur
            debut_date = (form.cleaned_data["debut_date"]).strftime('%Y-%m-%d')
            fin_date = (form.cleaned_data["fin_date"]).strftime('%Y-%m-%d')
            sock.write(json.dumps(["liste_cheques", ["date",debut_date,fin_date]] ))
            out = nk.full_read(sock)
            if not nk._is_success_code(out["retcode"]):
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))

            variables["list"] = out["msg"]["list"]
            variables["form"] = form

    variables["page"]["cheque"] = True
    return (variables, "note/trez_cheques.html")

@standard_page
def TresorerieAjoutRemise(request,sock,kwargs):
    """
        Page pour ajouter un crédit chèque à une remise
    """
    #TODO : factoriser le code pour permettre l'ajout d'autres moyens de paiement
    variables, dico = {}, {}
    dico.update(kwargs)
    variables.update(kwargs)
    action, idtransaction = kwargs["action"], kwargs["idtransaction"]

    #On vérifie qu'on a bien le droit ``tresorerie``
    sock.write(json.dumps(["mayi","tresorerie"]))
    out = nk.full_read(sock)
    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        variables["tresorerie"] = False
    else:
        variables["tresorerie"] = out["msg"]

    #On demande si on a le droit ``transactions_admin``
    sock.write(json.dumps(["mayi","transactions_admin"]))
    out = nk.full_read(sock)
    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        variables["transactions_admin"] = False
    else:
        variables["transactions_admin"] = out["msg"]

    if kwargs["action"] == "ajout" :
        if request.method == "POST" :
            form = forms.AjoutRemise(request.POST)
            idr = utilities._get_remise(sock)
            form.fields["idremise"].choices = [(i, "Remise {}".format(i)) for i in idr ]
            if form.is_valid() :
                dico.update(form.cleaned_data)
                sock.write(json.dumps(["ajout_remise",dico]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, messages.SUCCMSG_TRANSACTION_ADDED)
                    return HttpResponseRedirect('{}tresorerie/cheques/'.format(settings.NOTE_ROOT_URL))
                else:
                    messages.add_error(request, out["errmsg"])
                    return HttpResponseRedirect('{}tresorerie/cheques/'.format(settings.NOTE_ROOT_URL))
            else :
                variables["form"] = form
                variables["cheque"] = True
                return (variables, 'note/trez_ajout.html')
        else :
            sock.write(json.dumps(["transaction", idtransaction]))
            out = nk.full_read(sock)
            if not nk._is_success_code(out["retcode"]):
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect('{}index/'.format(settings.NOTE_ROOT_URL))

            initial_data = { "nom" : out["msg"]["spec_infos"]["nom"],
                             "prenom" : out["msg"]["spec_infos"]["prenom"],
                             "montant" : out["msg"]["trans_infos"]["montant"],
                             "banque" : out["msg"]["spec_infos"]["banque"],
                           }
            idr = utilities._get_remise(sock)
            form = forms.AjoutRemise(initial=initial_data)
            form.fields["idremise"].choices = [(i, "Remise {}".format(i)) for i in idr ]
            variables["form"] = form
            variables["cheque"] = True
        return (variables, 'note/trez_ajout.html')

    elif kwargs["action"] == "delete" :
        sock.write(json.dumps(["ajout_remise",dico]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            messages.add_success(request, messages.SUCCMSG_TRANSACTION_REMOVED)
            return HttpResponseRedirect('{}tresorerie/remises/'.format(settings.NOTE_ROOT_URL))
        else:
            messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect('{}index/'.format(settings.NOTE_ROOT_URL))

@standard_page
def TresorerieCloreRemise(request,sock,kwargs):
    """
        Affiche la page de clotûre d'une remise

        Dans ``kwargs`` :
         * idremise = <int>
         * clore = <str> peut être vide
    """
    variables = {}
    idremise, action = kwargs["idremise"], kwargs["clore"]
    variables["idremise"] = idremise

    #On vérifie qu'on a bien le droits ``tresorerie``
    sock.write(json.dumps(["mayi","tresorerie"]))
    out = nk.full_read(sock)
    if out["retcode"] != 0:
        messages.add_error(request, out["errmsg"])
        variables["tresorerie"] = False
    else:
        variables["tresorerie"] = out["msg"]

    if request.method == 'POST':
        form = request.POST
        if form.has_key("clore") :
            dico = { "idremise" : idremise,
                     "commentaire" : form["commentaire"],
                     "clore" : True,
                   }
            sock.write(json.dumps(["clore_remise",dico]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, messages.SUCCMSG_REMISE_CLOSED)
                return HttpResponseRedirect('{}tresorerie/remises/'.format(settings.NOTE_ROOT_URL))
            else:
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect('{}index/'.format(settings.NOTE_ROOT_URL))
        if form.has_key("sauvegarder"):
            dico = { "commentaire" : form["commentaire"],
                     "idremise" : idremise,
                     "clore" : False,
                   }
            sock.write(json.dumps(["clore_remise",dico]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                return HttpResponseRedirect('{}tresorerie/remises/{}'.format(settings.NOTE_ROOT_URL,idremise))
            else:
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect('{}index/'.format(settings.NOTE_ROOT_URL))
    else:
        sock.write(json.dumps(["liste_cheques",["idremise",idremise]]))
        out = nk.full_read(sock)
        if not nk._is_success_code(out["retcode"]):
            messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))
        variables["list"]=out["msg"]["list"][1]
        sock.write(json.dumps(["infos_remise",idremise]))
        out = nk.full_read(sock)
        initial_data = {"idremise" : idremise,
                        "commentaire" : out["msg"]["commentaire"],
                       }
        variables.update(out["msg"])
    variables["remise"] = True
    return(variables, 'note/trez_clore.html')

@standard_page
def TresorerieFacturation(request,sock,kwargs):
    """
        Affiche la page pour la création d'une facture
    """
    variables, dico = {"page" : {}}, {}

    #On vérifie qu'on a bien le droit ``tresorerie``
    sock.write(json.dumps(["mayi","tresorerie"]))
    out = nk.full_read(sock)
    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        variables["tresorerie"] = False
    else:
        variables["tresorerie"] = out["msg"]


    if request.method == 'POST':
        req = request.POST
        facture = forms.FactureForm(req)
        produits = formset_factory(forms.ProduitForm)
        produits = produits(req)
        if produits.is_valid() and facture.is_valid():
            dico.update(facture.cleaned_data)
            dico["liste_produits"] = produits.cleaned_data
            supreme = kwargs["variables_standard"]["whoami"]["supreme"]
            res, retcode, filename = render_tex(dico, "facture/facture_sample.tex", supreme)
            if retcode:
                messages.add_success(request, messages.SUCCMSG_INVOICE_ADDED)
                res = HttpResponse(res, content_type="application/pdf")
                res['Content-Disposition'] = "attachement; filename={}".format(filename)
                return res
            else:
                return HttpResponse(res, content_type="text/plain")

        else :
            variables["form"] = facture
            variables["formset"] = produits
    else :
        form = forms.FactureForm()
        formset = formset_factory(forms.ProduitForm)
        variables["form"] = form
        variables["formset"] = formset
    variables["page"]["facturation"] = True
    return (variables, "note/trez_facturation.html")

def teapot(request):
    """Easter egg"""
    page = """<html><head>\n<title>418 I'm a Teapot</title>\n</head><body>\n<h1>HTCPCP error 418: I'm a Teapot</h1>\nThe requested URL cannot provide coffee, because this is a networked teapot. See RFC2324.<p>\n<hr>\n"""
    #page += _display_versions()
    page += "</html>\n</body></html>"
    return HttpResponse(page, status=418)

@standard_page
def bad_request(request, sock, kwargs):
    """Page 400 customisée pour y ajouter des variables."""
    erreur = {
        "num" : 400,
        "name" : "Bad Request",
        "description" : "Le serveur n'est pas consentant et refuse de faire ce que tu lui demandes.",
    }
    variables = {
        "erreur" : erreur,
        "report_bugs_to" : settings.REPORT_BUGS_EMAIL,
        "NOTE_ROOT_URL" : settings.NOTE_ROOT_URL
    }
    return (variables, "handler_base.html")

@standard_page
def permission_denied(request, sock, kwargs):
    """Page 403 customisée pour y ajouter des variables."""
    erreur = {
        "num" : 403,
        "name" : "Forbidden",
        "description" : "Ton niveau d'accréditation n'est pas suffisant pour voir cette page.",
    }
    variables = {
        "erreur" : erreur,
        "report_bugs_to" : settings.REPORT_BUGS_EMAIL,
        "NOTE_ROOT_URL" : settings.NOTE_ROOT_URL
    }
    return (variables, "handler_base.html")

@standard_page
def page_not_found(request, sock, kwargs):
    """Page 404 customisée pour y ajouter des variables."""
    erreur = {
        "num" : 404,
        "name" : "Not Found",
        "description" : "Si tu as bidouillé l'URL pour arriver ici, ben t'es puni",
    }
    variables = {
        "erreur" : erreur,
        "report_bugs_to" : settings.REPORT_BUGS_EMAIL,
        "NOTE_ROOT_URL" : settings.NOTE_ROOT_URL
    }
    return (variables, "handler_base.html")

@standard_page
def server_error(request, sock, kwargs):
    """Page 500 customisée pour y ajouter des variables."""
    erreur = {
        "num" : 500,
        "name" : "Internal Server Error",
        "description" : "Le serveur n'a vraiment pas aimé ce que tu lui as demandé.",
    }
    variables = {
        "erreur" : erreur,
        "report_bugs_to" : settings.REPORT_BUGS_EMAIL,
        "NOTE_ROOT_URL" : settings.NOTE_ROOT_URL
    }
    return (variables, "handler_base.html")

def render_tex(data, template, supreme = False):
    """
        Fonction copiée et adaptée depuis le render_tex() du cr@ns.
        Créé un .pdf à partir d'un .tex

	Prend en entrée :
         * data = <dict> contenant les informations à insérer dans la
                  facture
         * template = <str> contenant le chemin au template.tex
         * supreme = <boolean>
    """
    file = template.rsplit('/', 1)[-1].rsplit('.', 1)[0]
    path = template.rsplit('/', 1)[0]
    filename = [datetime.today().date(), path, data["id_facture"], data["objet"] ]
    filename = "_".join([unicode(x).upper() for x in filename]).replace(" ","_").replace('"', "").replace("'", "").replace("/","_") + ".pdf"
    try:
        body = render_to_string(template, data).encode("utf-8")
    except TemplateDoesNotExist:
        raise Http404()

    # Création d'un répertoire temporaire
    TMP_PREFIX= os.path.join(settings.ROOT_PATH, "templates", path, '')
    tmp = mkdtemp(prefix=TMP_PREFIX)

    try:
        with open("{}/{}.tex".format(tmp, filename), "w") as f:
            f.write(body)
        del body

        # Si on compile pas deux fois, Satan chie sur ton pdf. Et il est moche.
        error = subprocess.Popen(
            ["pdflatex", "{}.tex".format(filename)],
            cwd=tmp,
            stdin=open(os.devnull, "r"),
            stderr=open(os.devnull, "wb"),
            stdout=open(os.devnull, "wb")
        ).wait()
        error = subprocess.Popen(
            ["pdflatex", "{}.tex".format(filename)],
            cwd=tmp,
            stdin=open(os.devnull, "r"),
            stderr=open(os.devnull, "wb"),
            stdout=open(os.devnull, "wb")
        ).wait()

        if error:
            if supreme : #Si on est respo-info
                log = open("{}/{}.log".format(tmp, filename)).read()
                return log, False, filename
            else:
                raise RuntimeError("pdflatex error (code {}) in {}/{}".format(error, tmp, filename))

        pdf = open("{}/{}.pdf".format(tmp, filename)).read()
    finally:
        shutil.rmtree(tmp)
        pass

    return pdf, True, filename

@standard_page
def activite_entree(request, sock, kwargs):
    """ Affiche la page Entrée de l'interface Pot
        Dans kwargs :
        * idact : id de l'activité qui correspond au pot
    """

    variables = {}

    #On récupère l'ensemble des droits dont dispose l'utilisateur
    sock.write(json.dumps(["mayi", "full_rights"]))
    out = nk.full_read(sock)

    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))
    else:
        droits =  out["msg"]["droits"]
        note, isadmin = "note" in droits, "activites_admin" in droits

    if not note:
       messages.add_error(request, _(u"Tu n'as pas le droit note !"))
       return HttpResponseRedirect("{}activites/".format(settings.NOTE_ROOT_URL))

    idpot = int(kwargs["idact"])
    pot = utilities._get_activite(sock, idpot, request, '{}activites/'.format(settings.NOTE_ROOT_URL))

    if not pot["ouvert"]:
        messages.add_error(request, _(u"Cette activité n'est pas ouverte !"))
        return HttpResponseRedirect("{}activites/".format(settings.NOTE_ROOT_URL))

    variables["idpot"] = idpot
    variables["nompot"] = pot["titre"]

    variables["credit_form"] = forms.CreditRetraitForm(prefix="credit_form", label_suffix=" :")
    variables["retrait_form"] = forms.CreditRetraitForm(prefix="retrait_form", label_suffix=" :")
    variables["transfert_form"] = forms.TransfertForm(prefix="transfert_form", label_suffix=" :")

    return (variables, "note/activite_entree.html")

@standard_page
def pot_gestion(request, sock, kwargs):
    """ Affiche la page de gestion des pots """
    variables = {}

    #On récupère l'ensemble des droits dont dispose l'utilisateur
    sock.write(json.dumps(["mayi", "full_rights"]))
    out = nk.full_read(sock)

    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))
    else:
        droits =  out["msg"]["droits"]
        hasadmin = "activites_admin" in droits

    if not hasadmin:
       messages.add_error(request, _(u"Tu n'as pas le droit activites_admin !"))
       return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))

    variables["hasadmin"] = hasadmin

    sock.write(json.dumps(["stats_entree_pot", 1790]))
    out = nk.full_read(sock)
    temp = out["msg"]
    temp = [ [l[0], l[1]] for l in temp]
    variables["liste"] = temp

    sock.write(json.dumps(["stats_entree_pot", 1754]))
    out = nk.full_read(sock)
    temp = out["msg"]
    temp = [ [l[0], l[1]] for l in temp]
    variables["liste1"] = temp


    return (variables, "note/pot_gestion.html")

@standard_page
def liste_droits(request, sock, kwargs):
    """
    Page qui affiche toute les personnes possédant des droits supérieurs à basic.

    But : Ne pas laisser des droits à des gens qui n'en ont
    plus besoin juste par oubli.
    """

    # On vérifie qu'on a bien le droit ``liste_droits``
    sock.write(json.dumps(["mayi", "full_rights"]))
    out = nk.full_read(sock)
    variables = {}
    # Si on a pas réussi à savoir si on a le droit
    if not nk._is_success_code(out["retcode"]):
        messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))
    else:
        user_rights = out["msg"]

    # Si on a pas le droit
    if "liste_droits" not in user_rights["droits"]:
        messages.add_error(request, _(u"Tu n'as pas le droit liste_droits."))
        return HttpResponseRedirect("{}index/".format(settings.NOTE_ROOT_URL))

    # Sinon on appelle la fonction de chargement du contenu de tableau
    sock.write(json.dumps(["liste_droits"]))
    out = nk.full_read(sock)

    # Si on a réussi à charger le tableau
    if nk._is_success_code(out["retcode"]):
        profils, vieux = out["msg"]
        # Formatage des droits
        for profil in profils:
            # On transforme en ensemble et on vire le droit basic (pas intéressant)
            droits = set(profil["droits"].split(',')) - set(["basic"])
            surdroits = set(profil["surdroits"].split(',')) - set(["basic"])

            # On va regrouper les droits/surdroits en 3 catégories : droits orphelins, droits surdroités et surdroits orphelins
            do, ds, so = droits-surdroits, droits & surdroits, surdroits - droits

            # Si on a 'all' surdroité alors on n'affiche que ça dans les deux premières catégories
            # Sinon on n'affiche que ça uniquement dans la catégorie droits orphelins"""
            if 'all' in ds:
                profil["droits"] = ""
                profil["droitsurdroits"] = 'all'
            else:
                profil["droits"] = 'all' if 'all' in do else ', '.join(do)
                profil["droitsurdroits"] = ', '.join(ds)

            profil["surdroits"] = ', '.join(so)

            # On ajoute des virgules entre les catégories selon si elles sont vides ou non
            if profil["droits"] and (profil["droitsurdroits"] or profil["surdroits"]):
                profil["droits"] = profil["droits"] + ", "
            elif profil["droitsurdroits"] and profil["surdroits"]:
                profil["droitsurdroits"] = profil["droitsurdroits"] + ", "

        # Si l'user possède au moins un surdroit on met les liens de modifications
            if user_rights["surdroits"] != []:
                profil["lienmodif"] = 'href = {}comptes/{}/modifier/listedroits#modifdroits'.format(settings.NOTE_ROOT_URL, profil["idbde"])

        # Enfin on range les gens selon leurs droits (tri personalisé subjectif)
        def ordre_droits(profil):
            """Fonction de classement des profils selon leurs droits : plus ils ont un droit fort plus ils sont haut.
            nodroitsurd permet de positionner les gens possédant des surdroits, en haut de leur catégorie"""

            droits = set(profil["droits"].split(', ') + profil["droitsurdroits"].split(', ') + profil["surdroits"].split(', '))
            supreme, nodroitsurd = 0.5*profil["supreme"], 0.5*(profil["droitsurdroits"]=="") + 0.25*(profil["surdroits"]=="")
            if supreme:
                return nodroitsurd
            elif 'all' in droits:
                return 1.5 - supreme
            elif droits & set(['comptes', 'search', 'adherents_weak', 'full_search']):
                return 2 + nodroitsurd
            elif 'overforced' in droits:
                return 3 + nodroitsurd
            elif 'forced' in droits:
                return 4 + nodroitsurd
            elif 'note' in droits:
                return 5 + (len(droits) == 1) + nodroitsurd
            else:
                return 7 + nodroitsurd


        profils = sorted(profils, key=ordre_droits)

        variables["profils"] = profils
        variables["nombre"] = len(profils)
        variables["vieux"] = [vieu['idbde'] for vieu in vieux]

        return (variables, "note/liste_droits.html")

    else:
        messages.add_error(request, out["errmsg"])
        return (variables, "note/liste_droits.html")
